<?php
	echo $this->Html->css('generic', null, array('block' => 'css'));
	echo $this->Html->css('assets/custom', null, array('block' => 'css')); 

	$this->Html->addCrumb(__('Groups'),array('controller' => 'groups', 'action' => 'index'));
	$this->Html->addCrumb(__('Group'), null);
?>
<div class="row-fluid">
	<!-- block -->
    <div class="block">
        <div class="navbar navbar-default" style="padding: 6px;">
            <div class="pull-left text-muted"><?php echo __('Groups'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
				<table class="table table-striped">
	              <tbody>
					<tr>
						<td><?php echo __('Id'); ?></td>
						<td><?php echo $this->Format->formatZeroEsq(h($group['Group']['id']),6); ?>&nbsp;</td>
					</tr>
					<tr>
						<td><?php echo __('Name'); ?></td>
						<td><?php echo h($group['Group']['name']); ?>&nbsp;</td>
					</tr>
	              </tbody>
	            </table>
            </div>
        </div>
    </div>
    <!-- /block -->
</div>
<?php $this->start('Acoes'); ?>
	<ul class="nav nav-list">
		<li class="nav-header"><?php echo __('Actions');?></li>
		<li class="divider"></li>
		<li><?php echo $this->Html->link(__('Edit Group'), array('action' => 'edit', $group['Group']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Group'), array('action' => 'delete', $group['Group']['id']), null, __('Are you sure you want to delete # %s?', $group['Group']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Groups'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('action' => 'add')); ?> </li>
		<li class="divider"></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
<?php 
	$this->end();

if (!empty($ent0205000['Ent0250000'])): ?>
<div class="row-fluid">
	<!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><?php echo __('Related Ent0250000s'); ?></div>
		    <div class="pull-right"><span class="badge badge-info"><?php echo count($ent0205000['Ent0250000']);?></span></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
				<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
					<thead>
						<tr>
							<th><?php echo __('Ent0235000 Id'); ?></th>
							<th><?php echo __('Ent0250002'); ?></th>
							<th><?php echo __('Ent0250003'); ?></th>
							<th><?php echo __('Ent0250001'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($ent0205000['Ent0250000'] as $ent0250000): ?>
						<tr>
							<td><?php echo $this->Html->link($ent0250000['Ent0235000']['ent0235001'], array('controller' => 'Ent0235000s', 'action' => 'view', $ent0250000['ent0235000_id']));; ?></td>
							<td style="text-align: right;"><?php echo $this->Format->formatMoeda($ent0250000['ent0250002']); ?></td>
							<td style="text-align: right;"><?php echo $this->Format->formatMoeda($ent0250000['ent0250003']); ?></td>
							<td style="text-align: right;"><?php echo $this->Format->formatMoeda($ent0250000['ent0250001']); ?></td>
							<td class="actions">
								<?php echo $this->Html->link(__('View'), array('controller' => 'ent0250000s', 'action' => 'view', $ent0250000['id']), array('class' => 'btn btn-mini', 'rule' => 'button')); ?>
								<?php if ($excluirOrcamento) { echo $this->Form->postLink(__('Delete'), array('controller' => 'ent0250000s', 'action' => 'delete', $ent0250000['id']), array('class' => 'btn btn-danger btn-mini', 'rule' => 'button'), __('Are you sure you want to delete # %s?', $ent0250000['id'])); } ?>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
            </div>
        </div>
    </div>
</div>
<?php endif;?>

<?php if (!empty($group['User'])): ?>
<div class="row-fluid">
	<!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><?php echo __('Related Users'); ?></div>
		    <div class="pull-right"><span class="badge badge-info"><?php echo count($group['User']);?></span></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
				<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
					<thead>
						<tr>
							<th><?php echo __('Id'); ?></th>
							<th><?php echo __('Name'); ?></th>
							<th><?php echo __('Username'); ?></th>
							<th><?php echo __('Status'); ?></th>
							<th><?php echo __('Lastlogin'); ?></th>
							<th><?php echo __('Created'); ?></th>
							<th><?php echo __('Modified'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($group['User'] as $user): ?>
						<tr>
							<td><?php echo $this->Format->formatZeroEsq($user['id'],6); ?></td>
							<td><?php echo $user['ent0060001']; ?></td>
							<td><?php echo $user['username']; ?></td>
							<td><?php echo $user['status']; ?></td>
							<td><?php echo (!empty($user['lastlogin'])) ? '&nbsp;' : $this->Format->formatDate($user['lastlogin']); ?></td>
							<td><?php echo $this->Format->formatDate($user['created']); ?></td>
							<td><?php echo $this->Format->formatDate($user['modified']); ?></td>
							<td class="actions">
								<?php echo $this->Html->link(__('View'), array('controller' => 'users', 'action' => 'view', $user['id']), array('class' => 'btn btn-mini', 'rule' => 'button')); ?>
								<?php echo $this->Html->link(__('Edit'), array('controller' => 'users', 'action' => 'edit', $user['id']), array('class' => 'btn btn-primary btn-mini', 'rule' => 'button')); ?>
								<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'users', 'action' => 'delete', $user['id']), array('class' => 'btn btn-danger btn-mini', 'rule' => 'button'), __('Are you sure you want to delete # %s?', $user['id'])); ?>
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
            </div>
        </div>
    </div>
</div>
<?php endif;?>