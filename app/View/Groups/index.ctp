<?php 
	echo $this->Html->css('datatable/jquery.dataTables.css', null, array('block' => 'css')); 
	echo $this->Html->script(array('datatables/jquery.dataTables'), array('block' => 'script')); 
	echo $this->Html->scriptBlock('
		jQuery(function($) {
		    	$("#example").dataTable({
			        "aaSorting": [[ 1, "asc" ]]
    			});
			});
		', 
		array('inline' => false ));
?>
<div class="row-fluid">
	<!-- block -->
    <div class="block">
        <div class="navbar navbar-default" style="padding: 6px;">
            <div class="pull-left text-muted"><?php echo __('Groups'); ?></div>
			<?php if (count($groups) > 0) :?>
		    <div class="pull-right"><span class="badge badge-info"><?php echo count($groups);?></span></div>
			<?php endif; ?>
        </div>
        <div class="block-content collapse in">
			<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
				<thead>
					<tr>
						<th><?php echo __('Id'); ?></th>
						<th><?php echo __('Name'); ?></th>

						<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($groups as $group): ?>
				<tr>
					<td><?php echo $this->Format->formatZeroEsq(h($group['Group']['id']),6); ?>&nbsp;</td>
					<td><?php echo h($group['Group']['name']); ?>&nbsp;</td>
					<td class="actions">
						<?php echo $this->Html->link(__('View'), array('action' => 'view', $group['Group']['id']), array('class' => 'btn btn-default btn-xs')); ?>
						<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $group['Group']['id']), array('class' => 'btn btn-primary btn-xs')); ?>
						<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $group['Group']['id']), array('class' => 'btn btn-danger btn-xs'), __('Are you sure you want to delete # %s?', $group['Group']['name'])); ?>
					</td>
				</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
        </div>
    </div>
</div>
<?php $this->start('Acoes'); ?>
	<ul class="nav">
		<li class="nav-header"><?php echo __('Actions');?></li>
		<li class="divider"></li>
		<li><?php echo $this->Html->link(__('New Group'), array('action' => 'add')); ?></li>
		<li class="divider"></li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
<?php $this->end(); ?>
