    <!-- Jssor Slider Begin -->
    <!-- You can move inline styles to css file or css block. -->
    <div id="slider2_container" style="position: relative; top: 0px; left: 0px; width: 495px; height: 335px; overflow: hidden; ">

        <!-- Loading Screen -->
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity:0.7; position: absolute; display: block;
                background-color: #000000; top: 0px; left: 0px;width: 100%;height:100%;">
            </div>
            <div class="div-block"></div>
        </div>

        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 495px; height: 335px; overflow: hidden;">
            <div>
				<?php echo $this->Html->image('estrutura/estrutura1.jpg', array('class' => '', 'alt' => ''));?>
            </div>
            <div>
                <?php echo $this->Html->image('estrutura/estrutura2.jpg', array('class' => '', 'alt' => ''));?>
            </div>
            <div>
                <?php echo $this->Html->image('estrutura/estrutura3.jpg', array('class' => '', 'alt' => ''));?>
            </div>
            <div>
                <?php echo $this->Html->image('estrutura/estrutura4.jpg', array('class' => '', 'alt' => ''));?>
            </div>
            <div>
                <?php echo $this->Html->image('estrutura/estrutura5.jpg', array('class' => '', 'alt' => ''));?>
            </div>            
        </div>
        <!-- bullet navigator container -->
        <div u="navigator" class="jssorb05" style="position: absolute; bottom: 16px; right: 6px;">
            <!-- bullet navigator item prototype -->
            <div u="prototype" style="POSITION: absolute; WIDTH: 16px; HEIGHT: 16px;"></div>
        </div>

        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora12l" style="width: 30px; height: 46px; top: 123px; left: 0px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora12r" style="width: 30px; height: 46px; top: 123px; right: 0px">
        </span>
    </div>
    <!-- Jssor Slider End -->