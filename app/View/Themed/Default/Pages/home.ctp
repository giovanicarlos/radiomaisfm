        <div id="conteudo">
            <div class="container">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="box-laranja">
                    	<p class="subtitulo"><span class="ico-mais-laranja"></span> A MAIS FM</p>
                        <p>A 103,9 agora é MAIS FM, a mais nova rádio do Sul de Minas, feita pra quem gosta de música que faz bem aos ouvidos.</p>
						<p>A programação é dedicada aos principais sucessos internacionais, além de pop rock nacional e MPB. Ou seja, música o tempo todo para você não precisar trocar de estação.</p>
						<p>Além disso, você ouve todo dia um Especial com 1 hora direto dos melhores artistas e bandas e programas feitos pra quem gosta de música ao vivo, acústica, dos anos 80 e 90 e muito mais! Curtiu? Então acompanhe todas as novidades da Mais FM e fique ligado na melhor seleção musical do Sul de Minas.</p>
                    </div>
                    <div class="box-cinza">
                    	<p class="subtitulo"><span class="ico-mais-cinza"></span> ESTRUTURA</p>
                        <?php include 'estrutura.ctp'; ?>
                    </div>                    
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="box-azul">
                    	<p class="subtitulo"><span class="ico-mais-azul"></span> ACOMPANHE AS NOVIDADES</p>
						<div class="fb-page" data-href="https://www.facebook.com/radiomaisoficial" data-width="514" data-height="656" data-hide-cover="false" data-show-facepile="true" data-show-posts="true"></div>
                    </div>
                </div>                
            </div>        
        </div>
        <div id="programas">
        	<div class="container">
            	<div class="col-md-12 col-sm-12 col-xs-12">
	            	<p class="subtitulo"><span class="ico-mais-roxo"></span> PROGRAMAS</p>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                	<div class="box-prog">
						<?php echo $this->Html->image('programas/prog01.png', array('class' => 'img-prog', 'alt' => 'Mais In Concert'));?>
                        <p class="titulo-prog">MAIS IN CONCERT</p>
                        <p class="horario-prog">Segunda, das 21 às 22h.</p>
                        <p class="descricao-prog">Uma hora com os maiores sucessos do Brasil e do mundo em versão ao vivo.</p>
                    </div>
                	<div class="box-prog">
                    	<?php echo $this->Html->image('programas/prog04.png', array('class' => 'img-prog', 'alt' => 'Mais Acústico'));?>
                        <p class="titulo-prog">MAIS ACÚSTICO</p>
                        <p class="horario-prog">Terça, das 21 às 22h.</p>
                        <p class="descricao-prog">Versões desplugadas de hits nacionais e internacionais, selecionados especialmente para os ouvintes da Mais FM.</p>
                    </div>    
                	<div class="box-prog">
                    	<?php echo $this->Html->image('programas/prog07.png', array('class' => 'img-prog', 'alt' => 'Mais 80'));?>
                        <p class="titulo-prog">MAIS 80</p>
                        <p class="horario-prog">Quarta, das 21 às 23h.</p>
                        <p class="descricao-prog">Duas horas com o melhor dos Anos 80 para você!</p>
                    </div>                                      
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                	<div class="box-prog">
                    	<?php echo $this->Html->image('programas/prog02.png', array('class' => 'img-prog', 'alt' => 'Mais 90'));?>
                        <p class="titulo-prog">MAIS 90</p>
                        <p class="horario-prog">Quinta, das 21 às 23h.</p>
                        <p class="descricao-prog">Uma hora com os maiores sucessos do Brasil e do mundo em versão ao vivo.</p>
                    </div>
                	<div class="box-prog">
						<?php echo $this->Html->image('programas/prog05.png', array('class' => 'img-prog', 'alt' => 'Toca Raul'));?>
                        <p class="titulo-prog">TOCA RAUL</p>
                        <p class="horario-prog">Sexta, das 21 às 23h.</p>
                        <p class="descricao-prog">O melhor do rock nacional de todos os tempos em duas horas de programação.</p>
                    </div>    
                	<div class="box-prog">
                    	<?php echo $this->Html->image('programas/prog08.png', array('class' => 'img-prog', 'alt' => 'Vale a Pena Ouvir de Novo'));?>
                        <p class="titulo-prog">VALE A PENA OUVIR DE NOVO</p>
                        <p class="horario-prog">Sábado, do meio-dia às 14h.</p>
                        <p class="descricao-prog">O melhor da programação e as mais pedidas pelos ouvintes.</p>
                    </div>                                      
                </div> 
                <div class="col-md-4 col-sm-12 col-xs-12">
                	<div class="box-prog">
                    	<?php echo $this->Html->image('programas/prog03.png', array('class' => 'img-prog', 'alt' => 'Embalos de Sábado a Noite'));?>
                        <p class="titulo-prog">EMBALOS DE SÁBADO A NOITE</p>
                        <p class="horario-prog">Sábado, das 22h à meia-noite.</p>
                        <p class="descricao-prog">Para embalar sua noite de sábado - desde os grandes clássicos da disco music até os maiores hits atuais.</p>
                    </div>
                	<div class="box-prog">
						<?php echo $this->Html->image('programas/prog06.png', array('class' => 'img-prog', 'alt' => 'Mais MPB'));?>
                        <p class="titulo-prog">MAIS MPB</p>
                        <p class="horario-prog">Domingo, das 10h ao meio-dia.</p>
                        <p class="descricao-prog">Uma hora com os maiores sucessos do Brasil e do mundo em versão ao vivo.</p>
                    </div>    
                	<div class="prog-completa">
                        <p class="titulo-prog">VEJA NOSSA PROGRAMAÇÃO COMPLETA</p>
                        <a href="#" class="btn btn-warning" data-target="#window-activation" data-toggle="modal">PROGRAMAÇÃO</a>
                    </div>                                      
                </div>                                
            </div>
        </div>
        <div id="top-artistas">
	        <div class="container">
            	<div class="col-md-12 col-sm-12 col-xs-12">
	            	<p class="subtitulo"><span class="ico-mais-laranja"></span> TOP ARTISTAS</p>
                </div>
            	<div class="col-md-2 col-sm-6 col-xs-12">
					<?php echo $this->Html->image('artistas/artista1.jpg', array('class' => 'img-responsive', 'alt' => 'The Beatles'));?>
	            	<p class="titulo-art">THE BEATLES</p>
                </div>    
            	<div class="col-md-2 col-sm-6 col-xs-12">
					<?php echo $this->Html->image('artistas/artista2.jpg', array('class' => 'img-responsive', 'alt' => 'Elton John'));?>
	            	<p class="titulo-art">ELTON JOHN</p>
                </div> 
            	<div class="col-md-2 col-sm-6 col-xs-12">
					<?php echo $this->Html->image('artistas/artista3.jpg', array('class' => 'img-responsive', 'alt' => 'Bee Gees'));?>
	            	<p class="titulo-art">BEE GEES</p>
                </div> 
            	<div class="col-md-2 col-sm-6 col-xs-12">
					<?php echo $this->Html->image('artistas/artista4.jpg', array('class' => 'img-responsive', 'alt' => 'Cold Play'));?>
	            	<p class="titulo-art">COLD PLAY</p>
                </div> 
            	<div class="col-md-2 col-sm-6 col-xs-12">
					<?php echo $this->Html->image('artistas/artista5.jpg', array('class' => 'img-responsive', 'alt' => 'Tears For Fears'));?>
	            	<p class="titulo-art">TEARS FOR FEARS</p>
                </div> 
            	<div class="col-md-2 col-sm-6 col-xs-12">
					<?php echo $this->Html->image('artistas/artista6.jpg', array('class' => 'img-responsive', 'alt' => 'Stive Wonder'));?>
	            	<p class="titulo-art">STIVE WONDER</p>
                </div>                                                                                                        
            </div>
        </div>