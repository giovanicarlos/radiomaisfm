<?php	$cakeDescription = __d('cake_dev', 'Rádio Mais FM'); ?>
<!DOCTYPE html>
<html class="no-js" prefix="og: http://ogp.me/ns#">
    <head>
	<?php 
		echo $this->Html->charset(); 
		echo $this->Html->meta('favicon.ico','img/favicon.ico',array('type' => 'icon'));
		echo $this->Html->meta('')
	?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>
		<?php echo $cakeDescription; ?>&nbsp;::&nbsp;<?php echo $title_for_layout; ?>
	</title>
	<?php // css
		echo $this->Html->css('bootstrap.min',array('media' => 'screen'));
		echo $this->Html->css('Glyphicon',array('media' => 'screen'));			
		echo $this->Html->css('UniversLTStdLightCn',array('media' => 'screen'));			
		echo $this->Html->css('site',array('media' => 'screen'));	
		echo $this->Html->css('jplayer',array('media' => 'screen'));			
	?>
	<?php // javascript
		echo $this->Html->script('jquery-1.9.1.min');
		echo $this->Html->script('jquery-ui.min-1.11.1');
		echo $this->Html->script('jquery.unobtrusive-ajax.min');
		echo $this->Html->script('jquery.validate.min');
		echo $this->Html->script('jquery.validate.unobtrusive.min');
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->script('JScript');
		echo $this->Html->script('util');
		echo $this->Html->script('jquery.blockUI');
		echo $this->Html->script('jquery.mask.min');
		echo $this->Html->script('jssor/jssor.slider.mini');
		echo $this->Html->script('jssor/jssor');			
		echo $this->Html->script('jquery.jplayer.min');		
//		$this->Html->scriptBlock($script, array('inline' => false));	
	?>
		<script type="text/javascript">
        //<![CDATA[
        $(document).ready(function(){
            var stream = {
                title: "Radio MAIS",
                mp3: "http://192.163.244.136:6014/;stream/1"
            },
            ready = false;
        
            $("#jquery_jplayer_1").jPlayer({
                ready: function (event) {
                    ready = true;
                    $(this).jPlayer("setMedia", stream);
                },
                pause: function() {
                    $(this).jPlayer("clearMedia");
                },
                error: function(event) {
                    if(ready && event.jPlayer.error.type === $.jPlayer.error.URL_NOT_SET) {
                        // Setup the media stream again and play it.
                        $(this).jPlayer("setMedia", stream).jPlayer("play");
                    }
                },
                swfPath: "jplayer",
                supplied: "mp3",
                preload: "none",
                wmode: "window",
                useStateClassSkin: true,
                autoBlur: false,
                keyEnabled: true
            });
			
		   $("#div-slider").change(function() {
			  var value = $("#slider-volume").val();
				$("#jquery_jplayer_1").jPlayer("volume", value/100);
		  
				if(value == 0)
				  {
					$("#vol").addClass("vol-mute");
				  }
				else
				  {
					$("#vol").removeClass("vol-mute").addClass("vol");
				  }
			});	
			
			window.setInterval(function () {inicializar();}, 5000);		
        });
		
		function inicializar(){
			 $.ajax({
				type: "GET",
				url: 'http://mais.com.br/xml-radio/playlist.xml',
				dataType: "xml",
				success: function (xml) { parseCarseXml(xml); },
				error: function() { }
			});
	    }
			
		function parseCarseXml(xml)
		{  
			var $xml = $(xml),
				name = $xml.find('Name').text();
				$("#musica").html(name);
		}		
        //]]>
        </script>    
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="scripts/html5shiv.min.js"></script>
		<script src="scripts/respond.min.js"></script>
	<![endif]-->
	<?php
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');		
	?>
    </head>
    
    <body>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&appId=1057262720954628&version=v2.3";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>   
        <div id="header">
            <div class="container">
                <div id="logo-mais-top" class="col-md-4 col-sm-12 col-xs-12">
					<?php echo $this->Html->image("logo-mais-fm.png", array("alt" => "Rádio Mais FM", 'url' => array('controller' => 'pages', 'action' => 'home'))); ?>
                </div>
                <div id="contato-top" class="col-md-4 col-sm-12 col-xs-12 text-right">
                	<span class="tel-cabecalho">Tel: (35) 3295-2651</span>
                	<span class="mail-cabecalho">contato@mais.com.br</span>                
                </div>
                <div id="social-top" class="col-md-4 col-sm-12 col-xs-12 text-right">
					<?php echo $this->Html->link(__('ANUNCIE'), 'javascript:void(0);', array('class' => 'btn btn-warning btn-anuncie', 'data-target' => '#window-anunciante', 'data-toggle' => 'modal'));?>
					<?php echo $this->Html->link('','http://www.facebook.com/sharer.php?u=http://jssor.com', array('class' => 'share-facebook', 'title' => 'Share on Facebook', 'target' => '_blank')); ?>
					<?php echo $this->Html->link('','http://twitter.com/share?url=http://jssor.com&text=jQuery%20Image%20Slider/Slideshow/Carousel/Gallery/Banner%20javascript+html%20TOUCH%20SWIPE%20Responsive', array('class' => 'share-twitter', 'title' => 'Share on Twitter', 'target' => '_blank')); ?>
                </div>                                          
            </div>            
        </div>
        <div id="barra-player">
        	<div class="container">
                <div id="jquery_jplayer_1" class="jp-jplayer"></div>
                <div id="jp_container_1" class="jp-audio-stream" role="application" aria-label="media player">
                    <div class="jp-type-single">
                        <div class="jp-gui jp-interface">
	                        <div class="col-md-7 col-sm-12 col-xs-12 separador-player musica-atual">
                                <a href"#" data-role="button" data-theme="a" class="jp-play">&nbsp;</a>
                                <span class="no-ar">NO AR: </span> <span id="musica"></span>
                            </div>
							<div id="div-volume" class="col-md-2 col-sm-12 col-xs-12 separador-player">
                                <div class="jp-volume-controls">
                                    <div class="ico-vol"></div>
                                    <div class="jp-volume-bar">
                                        <div class="jp-volume-bar-value"></div>
                                    </div>                                    
                                </div>
                            </div>
                            <div id="div-curtir" class="col-md-3 col-sm-12 col-xs-12 curtir-musica">
                                <span class="txt-curtiu">CURTIU ESSA MÚSICA? </span><a href="#" class="ico-curtir-musica"></a>
                            </div>                            
                        </div>
                    </div>
                </div>                                                       
            </div>
        </div>
        <div class="BarraFixa"></div>        
        
        <?php include 'vitrine.ctp'; ?>
        
        <div id="banner-principal">
        	<div class="container">
				<?php echo $this->Html->image('banners/banner01.jpg', array('class' => 'banner-principal-img img-responsive', 'alt' => 'Mont Blanc'));?>
			</div>
        </div>

		<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>

        <div id="footer">
            <div class="container">
                <div id="logo-mais-top" class="col-md-4 col-sm-12 col-xs-12">
					<?php echo $this->Html->image("logo-mais-fm.png", array("alt" => "Rádio Mais FM", 'url' => array('controller' => 'pages', 'action' => 'home'))); ?>
                </div>
                <div id="contato-top" class="col-md-4 col-sm-12 col-xs-12 text-right">
                	<span class="tel-cabecalho">Tel: (35) 3295-2651</span>
                	<span class="mail-cabecalho">contato@mais.com.br</span>                
                </div>
                <div id="social-top" class="col-md-4 col-sm-12 col-xs-12 text-right">
					<?php echo $this->Html->link(__('ANUNCIE'), 'javascript:void(0);', array('class' => 'btn btn-warning btn-anuncie', 'data-target' => '#window-anunciante', 'data-toggle' => 'modal'));?>
					<?php echo $this->Html->link('','http://www.facebook.com/sharer.php?u=http://jssor.com', array('class' => 'share-facebook', 'title' => 'Share on Facebook', 'target' => '_blank')); ?>
					<?php echo $this->Html->link('','http://twitter.com/share?url=http://jssor.com&text=jQuery%20Image%20Slider/Slideshow/Carousel/Gallery/Banner%20javascript+html%20TOUCH%20SWIPE%20Responsive', array('class' => 'share-twitter', 'title' => 'Share on Twitter', 'target' => '_blank')); ?>
                </div>                                          
            </div>                                         
        </div>        
        <div class="copyright">
            <div class="container">
                <div class="col-md-12 col-sm-12 col-xs-12 center-block">© Copyright 2014-2015 MAIS FM</div>
            </div>
        </div>

		<div class="modal fade" id="window-anunciante" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="ico-close"></span></button>                    
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel"><span class="ico-mais-laranja"></span> SEJA UM ANUNCIANTE NA MAIS FM</h4>
                        </div>
                        <div class="modal-body box-anunciante">
                     		<p>Quer ser um anunciante na MAIS FM? Entre em contato com o nosso departamento comercial. Será um prazer atendê-lo.</p>
                            <p class="txt-tel">(35) 3292-3955 </p> 
                            <p class="txt-email">comercial@mais.com.br</p>
                            <p>Av. Teixeira da Silva, 150, Sala 202, Jd. Aeroporto - Alfenas - MG</p>
                            <p>Se preferir, utilize nosso formulário de contato.</p>
                            <form class="form-horizontal">
                              <div class="form-group">
                                <label for="txtNome" class="col-sm-2 control-label">Nome:</label>
                                <div class="col-sm-10">
                                  <input type="text" class="form-control" id="txtNome" />
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="txtEmail" class="col-sm-2 control-label">E-mail:</label>
                                <div class="col-sm-10">
                                  <input type="text" class="form-control" id="txtEmail" />
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="txtTelefone" class="col-sm-2 control-label">Telefone:</label>
                                <div class="col-sm-10">
                                  <input type="text" class="form-control" id="txtTelefone" />
                                </div>
                              </div>       
                              <div class="form-group">
                                <label for="txtMensagem" class="col-sm-2 control-label">Mensagem:</label>
                                <div class="col-sm-10">
                                	<textarea class="form-control" id="txtMensagem"></textarea>
                                </div>
                              </div>                                                        
                              <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                  <button type="submit" class="btn btn-warning">ENVIAR</button>
                                </div>
                              </div>
                            </form>                         
                        </div>
                    </div>
                </div>
        </div>        

		<?php echo $this->Js->writeBuffer(); ?>
		
    </body>
</html>
