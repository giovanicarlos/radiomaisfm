<?php	$cakeDescription = __d('cake_dev', 'Rádio Mais FM'); ?>
<!DOCTYPE html>
<html class="no-js" prefix="og: http://ogp.me/ns#">
    <head>
	<?php 
		echo $this->Html->charset(); 
		echo $this->Html->meta('favicon.ico','img/favicon.ico',array('type' => 'icon'));
		echo $this->Html->meta('')
	?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>
		<?php echo $cakeDescription; ?>&nbsp;::&nbsp;<?php echo $title_for_layout; ?>
	</title>
	<?php // css
		echo $this->Html->css('bootstrap.min',array('media' => 'screen'));
		echo $this->Html->css('Glyphicon',array('media' => 'screen'));			
		echo $this->Html->css('UniversLTStdLightCn',array('media' => 'screen'));			
		echo $this->Html->css('site',array('media' => 'screen'));			
		echo $this->Html->css('adm',array('media' => 'screen'));			
	?>
	<?php // javascript
		echo $this->Html->script('jquery-1.9.1/jquery-1.9.1.min'); // jquery esta no diretorio webroot raiz e nao em themes
		echo $this->Html->script('jquery-ui.min-1.11.1');
		echo $this->Html->script('jquery.unobtrusive-ajax.min');
		echo $this->Html->script('jquery.validate.min');
		echo $this->Html->script('jquery.validate.unobtrusive.min.js');
		echo $this->Html->script('bootstrap.min');
		/*echo $this->Html->script('JScript.js');
		echo $this->Html->script('util');
		echo $this->Html->script('jquery.blockUI');
		echo $this->Html->script('jquery.mask.min');
		echo $this->Html->script('jssor/jssor.slider.mini');
		echo $this->Html->script('jquery.validate.min');
		echo $this->Html->script('jssor/jssor.js');*/
//		$this->Html->scriptBlock($script, array('inline' => false));	
	?>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="scripts/html5shiv.min.js"></script>
		<script src="scripts/respond.min.js"></script>
	<![endif]-->
	<?php
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');		
	?>
    </head>
    
    <body>
    <div>Giovani Teste</div>
        <div id="header">
            <div class="container">
                <div class="col-md-3 col-sm-12 col-xs-12">
					<?php echo $this->Html->image("logo-mais-fm.png", array("alt" => "Rádio Mais FM", 'url' => array('controller' => 'pages', 'action' => 'home'))); ?>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12">
                	<h1>Administração</h1>
                </div>
            </div>
        </div>
        <div class="BarraFixa"></div>        
        <div class="copyright">
            <div class="container">
                <div class="col-md-12 col-sm-12 col-xs-12 center-block">&nbsp;</div>
            </div>
        </div>
        
        <!--#Include File="vitrine.shtml" -->
        
        <div id="conteudo">
            <div class="container">
				<div class="row"> 
					<div class="col-md-3"> 
						<div class="well">
							<?php echo $this->fetch('Acoes');?>
						</div> 
					</div>
					<div class="col-md-9"> 
						<?php echo $this->Session->flash(); ?>
						<?php echo $this->fetch('content'); ?>
					</div>
				</div>
			</div>
        </div>

		

        <div class="copyright">
            <div class="container">
                <div class="col-md-12 col-sm-12 col-xs-12 center-block">© Copyright 2014-2015 MAIS FM</div>
            </div>
        </div>

		<div class="modal fade" id="window-anunciante" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="ico-close"></span></button>                    
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel"><span class="ico-mais-laranja"></span> SEJA UM ANUNCIANTE NA MAIS FM</h4>
                        </div>
                        <div class="modal-body box-anunciante">
                     		<p>Quer ser um anunciante na MAIS FM? Entre em contato com o nosso departamento comercial. Será um prazer atendê-lo.</p>
                            <p class="txt-tel">(35) 3292-3955 </p> 
                            <p class="txt-email">comercial@mais.com.br</p>
                            <p>Av. Teixeira da Silva, 150, Sala 202, Jd. Aeroporto - Alfenas - MG</p>
                            <p>Se preferir, utilize nosso formulário de contato.</p>
                            <form class="form-horizontal">
                              <div class="form-group">
                                <label for="txtNome" class="col-sm-2 control-label">Nome:</label>
                                <div class="col-sm-10">
                                  <input type="text" class="form-control" id="txtNome" />
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="txtEmail" class="col-sm-2 control-label">E-mail:</label>
                                <div class="col-sm-10">
                                  <input type="text" class="form-control" id="txtEmail" />
                                </div>
                              </div>
                              <div class="form-group">
                                <label for="txtTelefone" class="col-sm-2 control-label">Telefone:</label>
                                <div class="col-sm-10">
                                  <input type="text" class="form-control" id="txtTelefone" />
                                </div>
                              </div>       
                              <div class="form-group">
                                <label for="txtMensagem" class="col-sm-2 control-label">Mensagem:</label>
                                <div class="col-sm-10">
                                	<textarea class="form-control" id="txtMensagem"></textarea>
                                </div>
                              </div>                                                        
                              <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                  <button type="submit" class="btn btn-warning">ENVIAR</button>
                                </div>
                              </div>
                            </form>                         
                        </div>
                    </div>
                </div>
        </div>        

		<?php echo $this->Js->writeBuffer(); ?>
		
    </body>
</html>
