<?php
	$cakeDescription = __d('cake_dev', 'Cislagos');
?>
<!DOCTYPE html>
<html class="no-js">
    <head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription; ?>&nbsp;::&nbsp;
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $this->Html->script('jquery-1.9.1/jquery-1.9.1.min');

//		$this->Html->scriptBlock($script, array('inline' => false));	
	?>
	<!-- Bootstrap -->
	<?php
		echo $this->Html->css('bootstrap.min',array('media' => 'screen'));
		echo $this->Html->css('bootstrap-responsive.min',array('media' => 'screen'));
		echo $this->Html->css('assets/styles',array('media' => 'screen'));			
	?>
	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	<?php
		echo $this->Html->script('vendors/modernizr-2.6.2-respond-1.1.0.min');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');		
	?>
    </head>
	<body id="login">
		<div class="container">
			<div style="text-align: center;">
				<?php echo $this->Html->image('logo/cislagos06.svg', array('width' => '450')); ?>
			</div>
			</br>
			<?php echo $this->Session->flash(); ?>
			<?php echo $this->fetch('content'); ?>
		</div> <!-- /container -->
	<?php
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->script('assets/scripts.js');
		echo $this->Js->writeBuffer();
	?>
	 </body>
</html>
