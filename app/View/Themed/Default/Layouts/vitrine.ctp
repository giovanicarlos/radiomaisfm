    <!-- Jssor Slider Begin -->
    <!-- You can move inline styles to css file or css block. -->
    <div id="slider1_container" style="position: relative; margin: 0 auto;
        top: 0px; left: 0px; width: 1349px; height: 380px; overflow: hidden;">
        <!-- Loading Screen -->
        <div u="loading" style="position: absolute; top: 0px; left: 0px;">
            <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block;
                top: 0px; left: 0px; width: 100%; height: 100%;">
            </div>
            <div class="div-block"></div>
        </div>
        <!-- Slides Container -->
        <div u="slides" style="cursor: move; position: absolute; left: 0px; top: 0px; width: 1349px;
            height: 380px; overflow: hidden;">
            <div>
				<?php echo $this->Html->image('vitrine/vitrine1.png', array('class' => '', 'alt' => ''));?>
            </div>
            <div>
                <a href="#" data-target="#window-activation" data-toggle="modal"><?php echo $this->Html->image('vitrine/vitrine2.png', array('class' => '', 'alt' => ''));?></a>
            </div>
            <div>
                <a href="https://www.facebook.com/radiomaisoficial"><?php echo $this->Html->image('vitrine/vitrine3.png', array('class' => '', 'alt' => ''));?></a>
            </div>
        </div>
        <!-- bullet navigator container -->
        <div u="navigator" class="jssorb21" style="position: absolute; bottom: 26px; left: 6px;">
            <!-- bullet navigator item prototype -->
            <div u="prototype" style="POSITION: absolute; WIDTH: 19px; HEIGHT: 19px; text-align:center; line-height:19px; color:White; font-size:12px;"></div>
        </div>
        <!-- Bullet Navigator Skin End -->

        <!-- Arrow Left -->
        <span u="arrowleft" class="jssora21l" style="width: 55px; height: 55px; top: 123px; left: 8px;">
        </span>
        <!-- Arrow Right -->
        <span u="arrowright" class="jssora21r" style="width: 55px; height: 55px; top: 123px; right: 8px">
        </span>
    </div>
    <!-- Jssor Slider End -->