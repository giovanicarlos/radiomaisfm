﻿(function(jQuery){jQuery.fn.scrollFixed=function(params){params=jQuery.extend({appearAfterDiv:0,hideBeforeDiv:0},params);var element=jQuery(this);if(params.appearAfterDiv)var distanceTop=element.offset().top + jQuery(params.appearAfterDiv).outerHeight(true) + element.outerHeight(true);else var distanceTop = element.offset().top;if(params.hideBeforeDiv) var bottom = jQuery(params.hideBeforeDiv).offset().top - element.outerHeight(true) - 10;else var bottom = 200000;jQuery(window).scroll(function(){if(jQuery(window).scrollTop() > distanceTop && jQuery(window).scrollTop() < bottom){element.css({ 'position': 'fixed', 'top': '0px', 'z-index': '1000' });jQuery('.BarraFixa').fadeIn();}else{element.css({ 'position': 'static' });jQuery('.BarraFixa').hide();}});};})(jQuery);
function Falha() {
    alertify.alert('<strong>Ocorreu um erro inesperado.</strong><br />Sistema indisponível no momento. Por favor, tente novamente mais tarde.');
    $("input").prop("disabled", null);
    $("select").prop("disabled", null);
    $("textarea").prop("disabled", null);
    $(".btn").prop("disabled", null);

}

function Carregando() {
    $('.alerta').remove();
    $("input").prop("disabled", "disabled");
    $("select").prop("disabled", "disabled");
    $("textarea").prop("disabled", "disabled");
    $(".btn").prop("disabled", "disabled");
}

function Sucesso(data) {
    if (!data.Erro) {
        if (!data.NaoLimpa) {
            $("input:text").val("");
            $("input:password").val("");
            $("select").val("");
            $("textarea").val("");
        }
        if (data.Msg) {
            alertify.alert(data.Msg);
        }
        else if (data.Ajax) {
            $.ajax({
                type: "POST",
                url: data.Ajax,
                success: function (data) {
                    Sucesso(data);
                },
                complete: function () {
                    Completo();
                },
                beforeSend: function () {
                    Carregando();
                },
                error: function () {
                    Falha();
                }
            });
        }
        else if (data.Modal) {
            $('.modal').modal('show');
        }
        else {
            if (data.Url) {
                window.location = data.Url;
            } else {
                window.location = ".";
            }
        }
    }
    else {
        if (data.Msg) {
            alertify.alert(data.Msg);
            $("input").prop("disabled", null);
            $("select").prop("disabled", null);
            $("textarea").prop("disabled", null);
            $(".btn").prop("disabled", null);
        }
        else {
            alertify.alert('<strong>Ocorreu um erro inesperado!</strong><br />Sistema indisponível no momento. Por favor, tente novamente mais tarde.');
        }
    }
    CenterItem(".alerta");
}

function Completo() {
    $("input").prop("disabled", null);
    $("select").prop("disabled", null);
    $("textarea").prop("disabled", null);
    $(".btn").prop("disabled", null);
    $(".disabled").prop("disabled", "disabled");
    fechaalerta();
}
function fechaalerta() {
    $('.alerta').remove();
}
function CenterItem(theItem) {
    var w = $(window);
    $(theItem).css("left", (w.width() - $(theItem).width()) / 2 + w.scrollLeft() + "px");
    $(theItem).attr("onclick", "$('" + theItem + "').fadeOut(500)");
}
//Menu zebrado
function zebrarMenu() {
    if ($(window).width() < 751) {
        $('.ul-zebrado li:odd').addClass('zebraUm');
        $('.ul-zebrado li:even').addClass('zebraDois');
    }
    else {
        $('.ul-zebrado li').removeClass('zebraUm zebraDois');
    }
}
//$(function () {
//    $('input[placeholder], textarea[placeholder]').placeholder();
//});

$(function () {
    $(".tamanhomax").keyup(function (event) {
        var target = $("#contador-regressivo");
        var max = target.attr('title');
        var len = $(this).val().length;
        var restante = max - len;
        if (len > max) {
            var val = $(this).val();
            $(this).val(val.substr(0, max));
            restante = 0;
        }
        target.html(restante);
    });

    //Login
    $(".btn-topo-area-aluno").click(function () {
        $(".box-login").slideToggle();
    });

    $(window).resize(function () {
        zebrarMenu();
    });

    zebrarMenu();

    $(".center-block-passo").click(function () {
        $('.container-liberar-aula').effect("highlight", { color: "#facd59" }, 3000);
    });
	
	jQuery("#barra-player").scrollFixed();

});

