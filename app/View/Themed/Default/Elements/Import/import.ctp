<?php 
//	debug($this->params);
	echo $this->Html->css('chosen/chosen', null, array('block' => 'css')); 
	echo $this->Html->css('assets/DT_bootstrap', null, array('block' => 'css')); 
	echo $this->Html->script(array('datatables/jquery.dataTables'), array('block' => 'script')); 
	echo $this->Html->script(array('assets/DT_bootstrap'), array('block' => 'script'));
	echo $this->Html->script(array('chosen/chosen.jquery'), array('block' => 'script')); 
	echo $this->Html->scriptBlock('
		jQuery(function($) {
				$(".chosen-select").chosen();
		    	$("#example").dataTable({
			        "aaSorting": [[ 1, "asc" ]],
					"oLanguage": {
				      "sZeroRecords": "Não há registros para exibir. Escolha uma Unidade da Federação e um Município."
				    }
    			});
			});
		', 
		array('inline' => false ));
	echo $this->Html->scriptBlock('
		function importar(arquivo, modelo, retorno) {
			nomeobjRetorno = "return" + retorno;
			$.ajax({
				type: "GET",
				url: "'.$this->request->webroot.'" + modelo + "'.DS.'importarFile'.DS.'" + arquivo,
				success : function (dados) {
					alert(dados);
				}
			}); 
		}
	   ', 
		array('inline' => false)
	);
?>
<div class="row-fluid">
	<!-- block -->
	<div class="block">
		<div class="navbar navbar-inner block-header">
		    <div class="muted pull-left">Lista de arquivos para importação</div>
		</div>
		<div class="block-content collapse in">
		    <div class="span12">
				<?php if (count($files) > 0) :?>
				<table class="table table-striped">
		          <thead>
		            <tr>
		              <th>#</th>
		              <th>Nome do Arquivo</th>
					  <th>Ação</th>
					  <th>&nbsp;</th>
		            </tr>
		          </thead>
		          <tbody>
				<?php foreach ($files as $key => $file) :?>
		            <tr>
		              <td><?php echo $this->Format->formatZeroEsq(($key + 1),6); ?></td>
		              <td><?php echo $file['arquivo'];?></td>
					  <td><?php echo $this->Html->link(__('Importar'),'javascript:void(0);',array('class' => 'btn btn-primary', 'onClick' => 'importar("'.$file['arquivo'].'","'.$file['modelo'].'","'.($key + 1).'");'));?></td>
					  <td><div id='return<?php echo ($key + 1); ?>'><div></td>
		            </tr>
				<?php endforeach;?>
		          </tbody>
		        </table>
				<?php endif; ?>
		    </div>
		</div>
	</div>
	<!-- /block -->
</div>

<?php $this->start('Acoes'); ?>
<div class="well" style="max-width: 226px; padding: 8px 0px;">
	<ul class="nav nav-list">
		<li class="nav-header"><?php echo __('Actions');?></li>
		<li class="divider"></li>
		<li><?php echo $this->Html->link(__('New Ent0030000'), array('action' => 'add')); ?></li>
		<li class="divider"></li>
		<li><?php echo $this->Html->link(__('List Ent0020000s'), array('controller' => 'ent0020000s', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ent0020000'), array('controller' => 'ent0020000s', 'action' => 'add')); ?> </li>
		<li class="divider"></li>
		<li><?php echo $this->Html->link(__('List Ent0040000s'), array('controller' => 'ent0040000s', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ent0040000'), array('controller' => 'ent0040000s', 'action' => 'add')); ?> </li>
	</ul>
</div> 
<?php $this->end(); ?>
<?php echo $this->Js->writeBuffer(); ?>
