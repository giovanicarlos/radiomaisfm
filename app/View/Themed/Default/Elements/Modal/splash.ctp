<?php
	echo $this->Html->scriptBlock('
				var cSpeed=9;
				var cWidth=160;
				var cHeight=20;
				var cTotalFrames=13;
				var cFrameWidth=160;
				var cImageSrc=\''.$this->request->webroot.'img/sprites/sprites.gif'.'\';
				
				var cImageTimeout=false;
				var cIndex=0;
				var cXpos=0;
				var cPreloaderTimeout=false;
				var SECONDS_BETWEEN_FRAMES=0;
				
				function startAnimation(){
					document.getElementById(\'loaderImage\').style.backgroundImage=\'url(\'+cImageSrc+\')\';
					document.getElementById(\'loaderImage\').style.width=cWidth+\'px\';
					document.getElementById(\'loaderImage\').style.height=cHeight+\'px\';
					
					//FPS = Math.round(100/(maxSpeed+2-speed));
					FPS = Math.round(100/cSpeed);
					SECONDS_BETWEEN_FRAMES = 1 / FPS;
					
					cPreloaderTimeout=setTimeout(\'continueAnimation()\', SECONDS_BETWEEN_FRAMES/1000);
					
				}
				
				function continueAnimation(){
					
					cXpos += cFrameWidth;
					//increase the index so we know which frame of our animation we are currently on
					cIndex += 1;
					 
					//if our cIndex is higher than our total number of frames, we\'re at the end and should restart
					if (cIndex >= cTotalFrames) {
						cXpos =0;
						cIndex=0;
					}
					
					if(document.getElementById(\'loaderImage\'))
						document.getElementById(\'loaderImage\').style.backgroundPosition=(-cXpos)+\'px 0\';
					
					cPreloaderTimeout=setTimeout(\'continueAnimation()\', SECONDS_BETWEEN_FRAMES*1000);
				}
				
				function stopAnimation(){//stops animation
					clearTimeout(cPreloaderTimeout);
					cPreloaderTimeout=false;
				}
				
				function imageLoader(s, fun) { //Pre-loads the sprites image
					clearTimeout(cImageTimeout);
					cImageTimeout=0;
					genImage = new Image();
					genImage.onload=function (){cImageTimeout=setTimeout(fun, 0)};
					genImage.onerror=new Function(\'alert("Could not load the image")\');
					genImage.src=s;
				}
				
				//The following code starts the animation
				new imageLoader(cImageSrc, \'startAnimation()\');				
			', 
		array('inline' => true)
	);

?>

<!-- Splash Screen -->
<div class="modal fade" id="splashScreen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
		<div class="offset4">
			<div id="loaderImage"></div>
		</div>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
