<?php
	$tam = 206;
	if ($this->theme == "Cislagos") $tam = 412;
	echo $this->Html->css('chosen/chosen', null, array('block' => 'css')); 
	echo $this->Html->css('assets/custom', null, array('block' => 'css')); 
	echo $this->Html->script(array('chosen/chosen.jquery'), array('block' => 'script')); 
	echo $this->Html->script(array('jquery-meio-mask/meiomask'), array('block' => 'script'));
	echo $this->Html->scriptBlock('
			$(function() {
				$(".chosen").chosen({width : "'.$tam.'px"});

				$("#UserCPF").setMask("999.999.999-99");

				$("#UserCPF").blur(function(){
					var CPF = $(this).val();
/*					if (CPF.length > 0) {
						CPF = CPF.replace(/[^\d]+/g,"");
						$.ajax({
							type: "POST",
							url: "'.$this->request->webroot.'Ent0070000s'.DS.'buscar",
							data: {id:CPF},
							async: false,
							complete : function(result) {
								if (result.responseText == "1") {
									alert("CPF já cadastrado.");
								}
							}
						});
					} */
				});

	        });	', 
		array('inline' => false)
	);
	$this->Html->addCrumb(__('Users'),array('controller' => 'users', 'action' => 'index'));
	$this->Html->addCrumb(__('Edit User'), null);
?>
<div class="row-fluid">
	<!-- block -->
	<div class="block">
		<div class="navbar navbar-inner block-header">
			<div class="muted pull-left"><?php echo __('Add User'); ?></div>
		</div>
		<div class="block-content collapse in">
			<div class="span12">
				<?php echo $this->Form->create('User', array(
					'inputDefaults' => array(
						'div' => 'control-group',
						'label' => array(
							'class' => 'control-label'
						),
						'wrapInput' => 'controls'
					),
					'class' => 'form-horizontal'
				)); ?>
				<fieldset>
					<?php
//						echo $this->Form->input('CPF', array('placeholder' => 'Somente Números'));
						echo $this->Form->input('Ent0060000.ent0060001');
						echo $this->Form->input('group_id', array('class' => 'chosen'));
						echo $this->Form->input('username');
						echo $this->Form->input('email');
						echo $this->Form->input('ent0205000_id', array('empty' => 'Selecione...', 'class' => 'chosen'));
					?>
					<div class="form-actions">
						<?php echo $this->Form->submit(__('Submit'), array(
							'div' => false,
							'class' => 'btn btn-primary'
						)); ?>
						<?php echo $this->Html->link(__('Cancel'), array('action' => 'index'),array('class' => 'btn'));?>
					</div>
				</fieldset>
				<?php echo $this->Form->end(); ?>
			</div>
		</div>
	</div>
	<!-- /block -->
</div>
<?php $this->start('Acoes'); ?>
<div class="well" style="max-width: 226px; padding: 8px 0px;">
	<ul class="nav nav-list">
		<li class="nav-header"><?php echo __('Actions');?></li>
		<li class="divider"></li>
		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
		<li class="divider"></li>
		<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
		<li class="divider"></li>
		<li><?php echo $this->Html->link(__('List Ent0205000s'), array('controller' => 'ent0205000s', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ent0205000s'), array('controller' => 'ent0205000s', 'action' => 'add')); ?> </li>
	</ul>
</div> 
<?php $this->end(); ?>
<?php echo $this->Js->writeBuffer(); ?>
