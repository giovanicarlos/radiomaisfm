<?php 
	echo $this->Html->css('generic', null, array('block' => 'css'));
	echo $this->Html->css('assets/DT_bootstrap', null, array('block' => 'css')); 
	echo $this->Html->css('assets/custom', null, array('block' => 'css')); 
	echo $this->Html->script(array('datatables/jquery.dataTables'), array('block' => 'script')); 
	echo $this->Html->script(array('assets/DT_bootstrap'), array('block' => 'script'));
	echo $this->Html->scriptBlock('jQuery(function($) {
		    	$("#agendas").dataTable({
			        "aaSorting": [[ 0, "asc" ]],
					"oLanguage": {
				      "sZeroRecords": "Não há registros para exibir."
				    },
					"aoColumnDefs": [{ "bSortable": false, "aTargets": [2,4] }]  
    			});
			});
		', 
		array('inline' => false ));
	$this->Html->addCrumb(__('Users'),array('controller' => 'users', 'action' => 'index'));
	$this->Html->addCrumb(__('User'), null);
?>
<div class="row-fluid">
	<!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><?php echo __('User'); ?></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
				<table class="table table-striped">
					<tbody>
						<tr>
							<td><?php echo __('Id'); ?></td>
							<td><?php echo $this->Format->formatZeroEsq(h($user['User']['id']),6); ?>&nbsp;</td>
						</tr>
						<tr>
							<td><?php echo __('Status'); ?></td>
							<td><?php echo $this->Format->formatFlag($user['User']['status'],$user['User']['id'],'changeStatus','view');?>&nbsp;
							</td>
						</tr>
						<tr>
							<td><?php echo __('Group'); ?></td>
							<td><?php echo $this->Html->link($user['Group']['name'], array('controller' => 'groups', 'action' => 'view', $user['Group']['id'])); ?>&nbsp;</td>
						</tr>
						<tr>
							<td><?php echo __('Ent0060001'); ?></td>
							<td><?php echo h($user['Ent0060000']['ent0060001']); ?>&nbsp;</td>
						</tr>
						<tr>
							<td><?php echo __('Username'); ?></td>
							<td><?php echo h($user['User']['username']); ?>&nbsp;</td>
						</tr>
						<!--tr>
							<td><?php echo __('Ent0205000s'); ?></td>
							<td><?php echo $this->Html->link($user['Ent0205000']['ent0060001'], array('controller' => 'ent0205000s', 'action' => 'view', $user['Ent0205000']['id'])); ?>&nbsp;</td>
						</tr-->
						<tr>
							<td><?php echo __('Lastlogin'); ?></td>
							<td><?php echo h($user['User']['lastlogin']); ?>&nbsp;</td>
						</tr>
						<tr>
							<td><?php echo __('Created'); ?></td>
							<td><?php echo $this->Format->formatDate(h($user['User']['created'])); ?>&nbsp;</td>
						</tr>
						<tr>
							<td><?php echo __('Modified'); ?></td>
							<td><?php echo $this->Format->formatDate(h($user['User']['modified'])); ?>&nbsp;</td>
						</tr>
						<tr>
						</tr>
					</tbody>
	            </table>
            </div>
        </div>
    </div>
    <!-- /block -->
</div>

<?php $this->start('Acoes'); ?>
<div class="well" style="max-width: 226px; padding: 8px 0px;">
	<ul class="nav nav-list">
		<li class="nav-header"><?php echo __('Actions');?></li>
		<li class="divider"></li>
		<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?> </li>
		<li class="divider"></li>
		<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
		<li class="divider"></li>
		<li><?php echo $this->Html->link(__('List Ent0205000s'), array('controller' => 'ent0205000s', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ent0205000s'), array('controller' => 'ent0205000s', 'action' => 'add')); ?> </li>
</li>
	</ul>
</div> 
<?php $this->end(); ?>

<?php if (!empty($ent0210000['Ent0255000'])): ?>
<div class="row-fluid">
	<!-- block -->
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left"><?php echo __('Related Ent0255000s'); ?></div>
		    <div class="pull-right"><span class="badge badge-info"><?php echo count($ent0210000['Ent0255000']);?></span></div>
        </div>
        <div class="block-content collapse in">
            <div class="span12">
				<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="agendas">
					<thead>
						<tr>
							<th><?php echo __('Id'); ?></th>
							<th><?php echo __('Ent0255001'); ?></th>
							<th><?php echo __('Ent0255002'); ?></th>
							<th><?php echo __('Ent0215000 Id'); ?></th>
							<th class="actions"><?php echo __('Actions'); ?></th>
						</tr>
					</thead>
					<tbody>
					<?php foreach ($ent0210000['Ent0255000'] as $ent0255000): ?>
						<tr>
							<td><?php echo $this->Format->formatZeroEsq($ent0255000['id'],6); ?></td>
							<td><?php echo $ent0255000['ent0255001']; ?></td>
							<td style="text-align: center;"><?php echo $this->Format->formatFlag($ent0255000['ent0255002'],$ent0255000['id'],'changeStatus','view'); ?></td>
							<td><?php echo $this->Html->link($ent0255000['ent0060001'], array('controller' => 'ent0215000s', 'action' => 'view', $ent0255000['ent0215000_id'])); ?></td>
							<td class="actions">
								<?php echo $this->Html->link(__('View'), array('controller' => 'ent0255000s', 'action' => 'view', $ent0255000['id']), array('class' => 'btn btn-mini', 'rule' => 'button')); ?>
								<?php echo $this->Html->link(__('Edit'), array('controller' => 'ent0255000s', 'action' => 'edit', $ent0255000['id']), array('class' => 'btn btn-mini btn-primary', 'rule' => 'button')); ?>
								<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'ent0255000s', 'action' => 'delete', $ent0255000['id']), array('class' => 'btn btn-mini btn-danger', 'rule' => 'button'), __('Are you sure you want to delete # %s?', $ent0255000['id'])); ?>
							</td>
						</tr>
					<?php endforeach; ?>
					</tbody>
				</table>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>

