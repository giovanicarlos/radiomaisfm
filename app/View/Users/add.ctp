<?php
	echo $this->Html->css('chosen/chosen', null, array('block' => 'css')); 
	echo $this->Html->script(array('chosen/chosen.jquery'), array('block' => 'script')); 
?>
<div class="row-fluid">
	<!-- block -->
	<div class="block">
        <div class="navbar navbar-default" style="padding: 6px;">
			<div class="muted pull-left"><?php echo __('Add User'); ?></div>
		</div>
		<div class="block-content collapse in">
			<?php echo $this->Form->create('User', array(
				'inputDefaults' => array(
					'div' => 'form-group',
					'label' => array(
						'class' => 'col col-md-3 control-label'
					),
					'wrapInput' => 'col col-md-9',
					'class' => 'form-control'
				),
				'class' => 'well form-horizontal'
			)); ?>
			<?php
				echo $this->Form->input('name');
				echo $this->Form->input('group_id', array('class' => 'chosen'));
				echo $this->Form->input('username');
				echo $this->Form->input('email');
				echo $this->Form->input('password');
				echo $this->Form->input('password_confirm', array('type' => 'password'));
			?>
			<div class="form-group">
				<div class="col col-md-9 col-md-offset-3">
					<?php echo $this->Form->submit(__('Submit'), array(
								'div' => false,
								'class' => 'btn btn-primary'
							)); ?>
					<?php echo $this->Html->link(__('Cancel'), array('action' => 'index'),array('class' => 'btn btn-default'));?>
				</div>
			</div>
			<?php echo $this->Form->end(); ?>
		</div>
	</div>
	<!-- /block -->
</div>
<?php $this->start('Acoes'); ?>
	<ul class="nav nav-list">
		<li class="nav-header"><?php echo __('Actions');?></li>
		<li class="divider"></li>
		<li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
		<li class="divider"></li>
		<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
		<li class="divider"></li>
	</ul>
<?php $this->end(); ?>
<?php echo $this->Js->writeBuffer(); ?>
