<?php 
	echo $this->Html->script(array('datatables/jquery.dataTables'), array('block' => 'script')); 
	echo $this->Html->scriptBlock('
		jQuery(function($) {
				$(".chosen-select").chosen();
		    	$("#example").dataTable({
			        "aaSorting": [[ 1, "asc" ]],
					"oLanguage": {
				      "sZeroRecords": "Não há registros para exibir."
				    },
					"iDisplayLength": 25,
					"aoColumnDefs": [{ "bSortable": false, "aTargets": [5] }]  
    			});
			});
		', 
		array('inline' => false ));
?>

<div class="row-fluid">
	<!-- block -->
    <div class="block">
        <div class="navbar navbar-default" style="padding: 6px;">
            <div class="muted pull-left"><?php echo __('Users'); ?></div>
			<?php if (count($users) > 0) :?>
			    <div class="pull-right"><span class="badge badge-info"><?php echo count($users);?></span></div>
			<?php endif; ?>
        </div>
        <div class="block-content collapse in">
			<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
				<thead>
					<tr>
						<th><?php echo __(Inflector::humanize('id')); ?></th>
						<th><?php echo __(Inflector::humanize('name')); ?></th>
						<th><?php echo __(Inflector::humanize('group_id')); ?></th>
						<th><?php echo __(Inflector::humanize('status')); ?></th>
						<th class="actions"><?php echo __('Actions'); ?></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($users as $user): ?>
				<tr>
					<td><?php echo $this->Format->formatZeroEsq(h($user['User']['id']),6); ?>&nbsp;</td>
					<td>
						<?php echo h($user['Ent0060000']['ent0060001']); ?>
					</td>
					<td>
						<?php echo $this->Html->link($user['Group']['name'], array('controller' => 'groups', 'action' => 'view', $user['Group']['id'])); ?>
					</td>
					<td style='text-align: center;'><?php echo $this->Format->formatStatus($user['User']['status'],$user['User']['id']); ?></td>
					<td class="actions">
						<?php if ($this->theme == 'Cislagos') : ?>
							<?php echo $this->Html->link(null, array('action' => 'view', $user['User']['id']), array('class' => 'crud View', 'title' => 'Ver')); ?>
							<?php echo $this->Html->link(null, array('action' => 'edit', $user['User']['id']), array('class' => 'crud Edit', 'title' => 'Editar')); ?>
							<?php echo $this->Form->postLink(null, array('action' => 'delete', $user['User']['id']), array('class' => 'crud Delete', 'title' => 'Excluir'), __('Are you sure you want to delete # %s?', $user['User']['username'])); ?>
						<?php endif;?>
						<?php if ($this->theme == 'Standard'): ?>
							<?php echo $this->Html->link(__('View'), array('action' => 'view', $user['User']['id']), array('class' => 'btn btn-mini', 'rule' => 'button')); ?>
							<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $user['User']['id']), array('class' => 'btn btn-primary btn-mini', 'rule' => 'button')); ?>
							<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $user['User']['id']), array('class' => 'btn btn-danger btn-mini', 'rule' => 'button'), __('Are you sure you want to delete # %s?', $user['User']['username'])); ?>
						<?php endif; ?>
					</td>
				</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
        </div>
    </div>
</div>
<?php $this->start('Acoes'); ?>
	<ul class="nav nav-list">
		<li class="nav-header"><?php echo __('Actions');?></li>
		<li class="divider"></li>
		<li><?php echo $this->Html->link(__('New User'), array('action' => 'add')); ?></li>
		<li class="divider"></li>
		<li><?php echo $this->Html->link(__('List Groups'), array('controller' => 'groups', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Group'), array('controller' => 'groups', 'action' => 'add')); ?> </li>
	</ul>
<?php $this->end(); ?>
<?php echo $this->Js->writeBuffer(); ?>
