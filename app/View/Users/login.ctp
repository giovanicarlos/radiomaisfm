<?php echo $this->Form->create('User', array(
					'inputDefaults' => array(
						'div' => 'control-group',
						'label' => array(
							'class' => 'control-label'
						),
						'wrapInput' => 'controls'
					),
					'class' => 'form-signin'
				)); ?>
	<!-- h3 class="form-signin-heading"><?php echo __("Please sign in");?></h3 -->
</br></br>
		<?php 
			echo $this->Form->input('username', array('label' => false, 'placeholder' => __('Username'), 'style' => 'width: 93%;'));
			echo $this->Form->input('password', array('label' => false, 'placeholder' => __('Password'), 'style' => 'width: 93%;'));
		?>
		<!-- label class="checkbox" -->
			<?php // echo $this->Form->checkbox('remember');?> <?php // echo __("Remember password");?>
		<!-- /label -->
<?php echo $this->Form->submit(__('Enviar'), array('div' => false,'class' => 'btn btn-primary')); ?>
<br /></br />
<?php echo $this->Session->flash('auth'); ?>

