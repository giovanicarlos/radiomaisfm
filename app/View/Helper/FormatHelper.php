<?php
/* /app/View/Helper/LinkHelper.php */
App::uses('AppHelper', 'View/Helper');

class FormatHelper extends AppHelper {

	public $helpers = array('Html');

	// formatar numero de telefone
	public function formatPhone($phone) {
		if (empty($phone))
			return $this->output($phone);

		return $this->output(substr($phone,0,4).'-'.substr($phone,4,4));
	}

	// formatar Data
	public function formatDate($data) {
		if (empty($data))
			return $this->output($data);

		return $this->output(substr($data,8,2).'/'.substr($data,5,2).'/'.substr($data,0,4).substr($data,10,6));
	}

	// formatar data com hora e dia da semana (datatime)
	public function formatDataTime($data) {
		$semana = array(1 => 'Seg','Ter','Qua','Qui','Sex','Sáb','Dom');
		if (empty($data))
			return $this->output($data);

		$diaSemana = date('N',strtotime(substr($data,0,10)));

		return $this->output($semana[$diaSemana].', '.substr($data,8,2).'/'.substr($data,5,2).'/'.substr($data,0,4).substr($data,10,6));
	}

	// Converter hora para o formato hh:mm
	public function formatTime($hora) {
		return substr($hora,0,5);
	}

	// formatar o tipo de pessoa físico ou jurídico
	public function formatPessoa($pessoa) {
		if (empty($pessoa))
			return $this->output($pessoa);

		return ($pessoa == 'F') ? 'Pessoa Física' : 'Pessoa Jurídica';
	}

	// formatar o tipo de lançamento (C)rédito ou (D)ébito
	public function formatLancamento($tipo) {
		if (empty($tipo))
			return $this->output($tipo);

		return ($tipo == 'C') ? 'Crédito' : 'Débito';
	}

	// formatar o tipo para interno ou externo
	public function formatTipoImagem($tipo) {
		if (empty($tipo))
			return $this->output($tipo);

		return ($tipo == 'I') ? 'Interno' : 'Externo';
	}

	// formatar numeros como moedas
	public function formatMoeda($numero) {
		if (empty($numero))
			return $this->output($numero);

		return $this->output(number_format($numero, 2, ',', '.'));

	}

	// formatar CEP
	public function formatCEP($cep) {
		if (empty($cep)) 
			return $this->output($cep);

		return $this->output(substr($cep,0,5).'-'.substr($cep,5,3));
	}

	// formatar CNPJ
	public function formatCNPJ($cnpj) {
		if (empty($cnpj))
			return $this->output($cnpj);
		
		return $this->output(substr($cnpj,0,2).'.'.substr($cnpj,2,3).'.'.substr($cnpj,5,3).'/'.substr($cnpj,8,4).'-'.substr($cnpj,12,2));
	}

	// formatar CPF
	public function formatCPF($cpf) {
		if (empty($cpf))
			return $this->output($cpf);

		return $this->output(substr($cpf,0,3).'.'.substr($cpf,3,3).'.'.substr($cpf,6,3).'-'.substr($cpf,9,2));
	}

	// formatar Numero do Cartao Nacional de Saúde
	public function formatCNS($cns) {
		if (empty($cns))
			return $this->output($cns);

		return $this->output(substr($cns,0,3).' '.substr($cns,3,4).' '.substr($cns,7,4).' '.substr($cns,11,4));
	}

	// formatar numero de telefone
	public function formatTelefone($telefone) {
		switch (strlen($telefone)) {
			case 8 :
				return $this->output(substr($telefone,0,4).'-'.substr($telefone,4,4));
				break;
			case 10:
				return $this->output('('.substr($telefone,0,2).') '.substr($telefone,2,4).'-'.substr($telefone,6,4));
				break;
			default:
				return $this->output($telefone);
		}
	}

	// colocar zeros a esquerda
	public function formatZeroEsq($str,$tam) {
		return $this->output(str_pad($str, $tam, '0', STR_PAD_LEFT));
	}

	// formatar campo status colocando grafico em verde (1) e um grafico vermelho (0)	
	public function formatStatus($status,$id) {
		return ($status == '1') ? $this->Html->image('greenStatus.png', array('alt' => 'Ativo', 'title' => 'Ativo','url' => array('action' => 'changeStatus', $id))) : $this->Html->image('redStatus.png', array('alt' => 'Inativo','title' => 'Inativo','url' => array('action' => 'changeStatus', $id)));
	}

	// funcao derivada de formatStatus: inclui o parametro com o nome da action a ser executada
	public function formatFlag($flag,$id,$action,$retorno = null) {
		return ($flag == '1') ? $this->Html->image('greenStatus.png', array('alt' => 'Ativo', 'title' => 'Ativo','url' => array('action' => $action, $id, $retorno))) : $this->Html->image('redStatus.png', array('alt' => 'Inativo','title' => 'Inativo','url' => array('action' => $action, $id, $retorno)));
	}	
	

	// formatar campos do tipo (S)im / (N)ão
	public function formatSimNao($campo) {
		return ($campo == 'S') ? 'Sim' : 'Não';
	}

	// funcao complementar para trocar letras acentuadas minusculas por maiusculas	
	public function formatMaiusculo ($texto) {
		$aux = array('â' => 'Â', 'á' => 'Á', 'ã' => 'Ã', 'à' => 'À', 'ä' => 'Ä', 'é' => 'É', 'ê' => 'Ê', 'í' => 'Í', 'ì' => 'Ì', 'ó' => 'Ó', 'õ' => 'Õ', 'ô' => 'Ô', 'ú' => 'Ú', 'ù' => 'Ù', 'ç' => 'Ç');

		foreach ($aux as $key => $val) 
			$texto = str_replace($key, $val, $texto);
		
		return $this->output($texto);
	}

	# Os parâmetros da função ordinal:
	# numero - É o número que você quer transformar em ordinal
	# genero - um string contendo "a" ou "o" para definir o gênero.
	# maiusculas - true para primeiras letras em caixa alta.
	
	# A construção dos ordinais eu fiz segundo explicação do Vestibuol.

	public function ordinal($numero, $genero, $maiusculas = false) {
		$numero = $this->formatZeroEsq($numero,20);
  
		$elementos[1] = Array("", "primeir", "segund", "terceir", "quart", "quint", "sext", "sétim", "oitav", "non");
		$elementos[2] = Array("", "décim", "vigésim", "trigésim", "quadragésim", "quinquagésim", "sexagésim", "septuagésim", "octogésim", "nonagésim");
		$elementos[3] = Array("", "centésim", "ducentésim", "trecentésim", "quadringentésim", "quingentésim", "seiscentésim", "septingentésim", "octingentésim", "nongentésim");
		$elementos[4] = "milésim";
		$elementos[7] = "milhonésim";
		$elementos[10] = "bilhonésim";
		$elementos[13] = "trilhonésim";
  
		$controle = 3;
		$ordinal = "";
		$soma = 0;
  
		for ($c = 5; $c <= 19; $c++) {
			$num = substr($numero, $c, 1);
    		settype($num, "integer");
     
    		if ($num <> 0 && ($num > 1 || $c > 16)) {
				$temp_ord = $elementos[$controle][$num];
              
			if ($maiusculas)
				$temp_ord = strtoupper(substr($temp_ord,0,1)).substr($temp_ord,1,strlen($temp_ord)-1);
        
			$ordinal = $ordinal." ".$temp_ord.$genero;
        
			$soma+= $num*10^$controle;
        } else if ($num <> 0) {
	        $soma+= $num*10^$controle;           
        }
		
		$controle--;
     
		if ($controle == 0 && $c < 19) {
	        if ($soma > 1 && isset($elementos[20-$c])) { 
				$temp_ord = $elementos[20-$c];
              
				if ($maiusculas)
					$temp_ord = strtoupper(substr($temp_ord,0,1)).substr($temp_ord,1,strlen($temp_ord)-1);
        
					$ordinal = $ordinal." ".$temp_ord.$genero;
				}
           
				$controle = 3;
				$soma = 0;
			}
		}   
		return $this->output($ordinal);
	}

	public function data_extenso ($data = false) {
		if ($data)	{
			$mes = date('m', strtotime($data));
	    } else {
			$mes = date('m');
			$data = date('Y-m-d');
		}
		$meses = array (
			'01' => 'Janeiro',
			'02' => 'Fevereiro',
			'03' => 'Março',
			'04' => 'Abril',
			'05' => 'Maio',
			'06' => 'Junho',
			'07' => 'Julho',
			'08' => 'Agosto',
			'09' => 'Setembro',
			'10' => 'Outubro',
			'11' => 'Novembro',
			'12' => 'Dezembro'
		);
		$dias = array (
			0 => 'Domingo',
			1 => 'Segunda-feira',
			2 => 'Terça-feira',
			3 => 'Quarta-feira',
			4 => 'Quinta-feira',
			5 => 'Sexta-feira',
			6 => 'Sábado'
		);
		return $dias[date('w', strtotime($data))] . ', ' . date('d', strtotime($data)) . ' de ' . $meses[$mes] . ' de ' . date('Y', strtotime($data));
	}

}
?>
