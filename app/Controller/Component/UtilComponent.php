<?php
# -------------------------------------------
# Gerar senhas Aleatórias
# @author&nbsp;&nbsp;&nbsp; Thiago Belem <contato@thiagobelem.net>
#
# @param integer $tamanho Tamanho da senha a ser gerada
# @param boolean $maiusculas Se terá letras maiúsculas
# @param boolean $numeros Se terá números
# @param boolean $simbolos Se terá símbolos
#
# @return string A senha gerada
# -------------------------------------------

class UtilComponent extends Component {

	# -------------------------------------------
	# Gerar senhas Aleatórias
	# @author Thiago Belem <contato@thiagobelem.net>
	#
	# @param integer $tamanho Tamanho da senha a ser gerada
	# @param boolean $maiusculas Se terá letras maiúsculas
	# @param boolean $numeros Se terá números
	# @param boolean $simbolos Se terá símbolos
	#
	# @return string A senha gerada
	# -------------------------------------------
	public function gerarSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false) {
		$lmin = 'abcdefghijklmnopqrstuvwxyz';
		$lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$num = '1234567890';
		$simb = '!@#$%*-';
		$retorno = '';
		$caracteres = '';

		$caracteres .= $lmin;
		if ($maiusculas) $caracteres .= $lmai;
		if ($numeros) $caracteres .= $num;
		if ($simbolos) $caracteres .= $simb;

		$len = strlen($caracteres);
		for ($n = 1; $n <= $tamanho; $n++) {
			$rand = mt_rand(1, $len);
			$retorno .= $caracteres[$rand-1];
		}
		return $retorno;
	}
	
	# -------------------------------------------
	# Retornar o nome do Mês abreviado ou por extenso
	# @author Cleiton Andrade <andrade.cleiton@gmail.com>
	#
	# @param  integer $mes  -> número de 1 a 12 correspondente ao mes
	# @param  string  $tipo -> 'E' para retornar o mes por extenso ou 'A' para retornar o mes abreviado 
	#
	# @return string -> O nome do mês por extenso ou abreviado de acordo com paramentro de entrada
	# -------------------------------------------
	public function meses($mes, $tipo = 'E') {
		$extenso = array(1 => 'Janeiro','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro');
		$abreviado = array(1 => 'Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez');
		
		$mes = (int)$mes;
		
		if (($mes >= 1) and ($mes <= 12)) {
			if ($tipo == 'E') 
				return ($extenso[$mes]);
			else
				return ($abreviado[$mes]); 	
		}		
	}

	# -------------------------------------------
	# Retornar o nome do dia da semana abreviado ou por extenso
	# @author Cleiton Andrade <andrade.cleiton@gmail.com>
	#
	# @param  integer $dia  -> número de 1 a 7 correspondente ao dia da semana
	# @param  string  $tipo -> 'E' para retornar o mes por extenso ou 'A' para retornar o mes abreviado 
	#
	# @return string -> O nome do dia da semana por extenso ou abreviado de acordo com paramentro de entrada
	# -------------------------------------------
	public function dias($dia, $tipo = 'E') {
		$extenso = array(1 => 'Domingo','Segunda-Feira','Terça-Feira','Quarta-Feira','Quinta-Feira','Sexta-Feira','Sábado');
		$abreviado = array(1 => 'Dom','Seg','Ter','Qua','Qui','Sex','Sáb');
		
		$dia = (int)$dia;
		
		if (($dia >= 1) and ($dia <= 12)) {
			if ($tipo == 'E') 
				return ($extenso[$dia]);
			else
				return ($abreviado[$dia]); 	
		}		
	}

	# -------------------------------------------
	# Retirar a mascara de entrada de dados
	# @author Cleiton Andrade <andrade.cleiton@gmail.com>
	#
	# @param  string $field -> campo textual com mascara
	#
	# @return string -> Campo sem mascaras
	# -------------------------------------------

	public function removeMasks($field) {
		$mascaras = array(' ','{','}','[',']','-','.',',','/','|','\\');

		foreach ($mascaras as $val) {
			$pos = strpos($field,$val);
			if (!$pos === false) {
				$field = str_replace($val,'',$field);
			}
		}

		return (trim($field));
	}

	# -------------------------------------------
	# Converter datas para o padrao Y-m-d
	# @author Cleiton Andrade <andrade.cleiton@gmail.com>
	#
	# @param  string $dateString -> string contendo uma data válida
	#
	# @return date -> Data no formato 'Y-m-d'
	# -------------------------------------------

	public function dataYMD($dateString) {
		return (substr($dateString,6,4).'-'.substr($dateString,3,2).'-'.substr($dateString,0,2));
	}

	# -------------------------------------------
	# Converter datas para o padrao d-m-Y
	# @author Cleiton Andrade <andrade.cleiton@gmail.com>
	#
	# @param  string $dateString -> string contendo uma data válida
	#
	# @return date -> Data no formato 'd-m-Y'
	# -------------------------------------------

	public function dataDMY($dateString,$separador = null) {
		if (is_null($separador)) $separador = '-';
		return (substr($dateString,8,2).$separador.substr($dateString,5,2).$separador.substr($dateString,0,4));
	}

	# -------------------------------------------
	# Converter Numeros com ponto flutuante para o formato 999.99
	# @author Cleiton Andrade <andrade.cleiton@gmail.com>
	#
	# @param  string $numero -> string contendo um numero (float) válido
	#
	# @return string -> numero formatado para 999,99
	# -------------------------------------------

	public function numeroDecimal($numero) {
		return (str_replace(',','.',(str_replace('.','',$numero))));
	}

	# -------------------------------------------
	# Converter strings de horas no formato HH:MM:SS para HH:MM
	# @author Cleiton Andrade <andrade.cleiton@gmail.com>
	#
	# @param  string $time -> string contendo uma hora válida no formato HH:MM:SS
	#
	# @return string -> string contendo uma hora válida no formato HH:MM
	# -------------------------------------------

	public function timeHHMM($hora) {
		return (substr($hora,0,5));
	}
}
?>
