<?php
// Arquivo      : Implementacao de classe para manipulacao de strings
//
// Classe       : FoneticaComponent 
// Metodos      : tiraracentos    -> tirar acentos de palavras
//              : filtracaracteres-> filtrar somente caracteres validos  
//              : subst           -> substituicao de carateres 
//              : substseantes    -> substituir uma substring antes de outra substring 
//              : fonetica        -> recebe um texto e retorna uma string fonetica
//              
// Data Criacao : 14/07/2006
// Data Revisao : 26/07/2006
// by Andrade

class FoneticaComponent extends Component {
  
	// metodo para tirar acentos de palavras
	public function tiraracentos($palavra) {
		$a = array('á','à','â','ã','ä','å','Á','Â','Ã','À','Ä','Å');
		$e = array('é','ê','è','ë','É','Ê','È','Ë');
		$i = array('í','î','ì','ï','Í','Î','Ì','Ï');
		$o = array('ó','ô','õ','ò','ö','Ó','Ô','Õ','Ò','Ö');
		$u = array('ú','û','ù','ü','Ú','Û','Ù','Ü');
		$c = array('ç','Ç');
		$n = array('ñ','Ñ');
		$y = array('ý','ÿ','Ý','',);
	
		$palavra = str_replace($a,'A',$palavra);
		$palavra = str_replace($e,'E',$palavra);
		$palavra = str_replace($i,'I',$palavra);
		$palavra = str_replace($o,'O',$palavra);
		$palavra = str_replace($u,'U',$palavra);
		$palavra = str_replace($c,'C',$palavra);
		$palavra = str_replace($n,'N',$palavra);
		$palavra = str_replace($y,'Y',$palavra);
	
		return ($palavra);
	}
  
	// metodo para filtrar somente caracteres validos
	public function filtracaracteres($palavra, $caracteres){
		$aux = '';
		for($indice = 0; $indice <= strlen($palavra); $indice++) {
			if (in_array(substr($palavra,$indice,1),$caracteres))
		    $aux = $aux.substr($palavra,$indice,1); 
		}
		return ($aux);
	}
  
	// metodo de substituicao
	public function subst($texto, $procurar, $substituir) {
    	$tam = strlen($procurar);
		$tam_aux = strlen($texto);
		$pos = strpos($texto,$procurar);
	
		while (!($pos === false)) {
			if ($pos == 0)
	    		$texto = $substituir.substr($texto,$tam,($tam_aux - $tam));
		
	  		if ($pos > 0)	
        		$texto = substr($texto,0,$pos).$substituir.substr($texto,($pos + $tam),($tam_aux - ($pos + $tam)));
      
	  		$tam_aux = strlen($texto);
      		$pos = strpos($texto,$procurar);
		}
	
		return ($texto);
  	}
  
  	// metodo para substituir uma substring antes de outra substring passada como parametro
  	public function substseantes($texto, $procurar, $substituir, $antesde) {
    	$aux = '';
    	for ($indice = 0; $indice <= strlen($texto); $indice++) {
			if (substr($texto,$indice,1) == $procurar) {
	    		if (in_array(substr($texto,($indice + 1),1),$antesde))
					$aux = $aux.$substituir;
				else
		  			$aux = $aux.  substr($texto,$indice,1);
	  		}
	  		else
	    		$aux = $aux.substr($texto,$indice,1);
		}
    	return ($aux);  
  	}
  
  	// metodo que recebe um texto e retorna uma string fonetica
  	public function fonetica($texto) {
    	$caractervalido = array(0 => 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','0','1','2','3','4','5','6','7','8','9',' ');
		// converter para maiusculas
	    $aux = strtoupper($texto);

		// Substituir 'ç' por 's'
		$aux = $this->subst($aux,'Ç','S');

		// tirar acentos
		$aux = $this->tiraracentos($aux);
	
		// filtra caracteres considerando apenas letras e numeros
		$aux = $this->filtracaracteres($aux,$caractervalido);
	
		// substituir 'ph' por 'f'
		$aux = $this->subst($aux,'PH','F');
	
		// substituir 'SCH' por 'C' (trocar com X: Ex.: Schimenes)
		$aux = $this->subst($aux,'SCH','C');
	
		// excluir 'H'
		$aux = $this->subst($aux,'H','');
	
		// substituir 'Z' por 'S'
		$aux = $this->subst($aux,'Z','S');
	
		// substituir 'X' por 'C'
		$aux = $this->subst($aux,'X','C');
	
		// substituir 'Y' por 'I'
		$aux = $this->subst($aux,'Y','I');
	
		// substituir 'W' por 'V'
		$aux = $this->subst($aux,'W','V');
	
		// substituir 'K' por 'C'
		$aux = $this->subst($aux,'K','C');
	
		// substituir 'QU' por 'C'
		$aux = $this->subst($aux,'QU','C');
	
		// substituir 'IRT' e 'ERT' por 'IUT' e 'EUT' 
		$aux = $this->subst($aux,'IRT','IUT');
		$aux = $this->subst($aux,'ERT','EUT');
	
		// substituir 'G' antes de 'T'
		$aux = $this->subst($aux,'GT','T');
	
		// substituir 'C' antes de 'T'
		$aux = $this->subst($aux,'CT','T'); 
	
		// adicionando um espaço no final para as regras a seguir
		$aux = $aux.' ';
	
		// retirar de, da, do, dos, das, d', e
		$aux = $this->subst($aux,' DE ',' ');
		$aux = $this->subst($aux,' DA ',' ');
		$aux = $this->subst($aux,' DO ',' ');
		$aux = $this->subst($aux,' D ',' ');
		$aux = $this->subst($aux,' DOS ',' ');
		$aux = $this->subst($aux,' DAS ',' ');
		$aux = $this->subst($aux,' E ',' ');

	    // substituir 'N' no final por 'M'
		$aux = $this->subst($aux,'N ','M ');
	
		// substituir 'C' antes de 'E' e 'I' por 'S'
		$aux = $this->subst($aux,'CE','SE');
		$aux = $this->subst($aux,'CI','SI');
	
		// substituir 'GIU' por 'JU'
		$aux = $this->subst($aux,'GIU','JU');
	
		// substituir 'GEO' por 'JO'
		$aux = $this->subst($aux,'GEO','JO');
	
		// substituir 'G' antes de 'E' e 'I' pro 'J'
		$aux = $this->subst($aux,'GE','JE');
		$aux = $this->subst($aux,'GI','JI');
	
		// substituir 'I' e 'E' no final por 'A'
		$aux = $this->subst($aux,'E ','A ');
		$aux = $this->subst($aux,'I ','A ');
	
		// substituir 'UI' ou 'EU' no inicio por 'VI' ou 'VE'
		if (strlen($aux) >= 2 ) {
			if ((substr($aux,0,2) == 'UI') || (substr($aux,0,2) == 'UI'))
	    		$aux = 'V'.substr($aux,1,(strlen($aux)));
		}	
	
		// substituir 'N' antes de 'P' e 'B' por 'M'
		$aux = $this->subst($aux,'NP','MP');
		$aux = $this->subst($aux,'NB','MB');
	
		// substituir 'M' ands de consoantes diferentes de 'P' e 'B' por 'N'
		$consoantesmenosPB = array(0 =>'C','D','F','G','H','J','L','M','N','Q','R','S','T','V','X','Z','W','K','Y');
		$aux = $this->substseantes($aux,'M','N',$consoantesmenosPB);
	
		// substituir 'L' antes de consoantes, menos 'L', por 'U'
		$consoantesmenosL = array(0 =>'B','C','D','F','G','H','J','M','N','P','Q','R','S','T','V','X','Z','W','K','Y');
		$aux = $this->substseantes($aux,'L','U',$consoantesmenosL);
	
		// tira vogais no inicio dos nomes
		// esta regra tera que ser revista
		$vogais = array(0 => 'A','E','I','O','U');
		$consoantes = array(0 => 'B','C','D','F','G','H','J','K','L','M','N','P','Q','R','S','T','V','X','Y','W','Z');
	
		if (strlen($aux) >= 2) {
			if (in_array(substr($aux,0,1),$vogais) && in_array(substr($aux,1,1),$consoantes))
				$aux = substr($aux,1,strlen($aux));
		}
	
		// tirar letras dobradas
		if (strlen($aux) >= 2) {
	  		$t = '';
	  		for ($indice = 0; $indice <= (strlen($aux)); $indice++ ) {
	    		if (substr($aux,$indice,1) != substr($aux,($indice + 1),1)) {
		  			$t = $t.substr($aux,$indice,1);
				}  
	  		}
	  		$aux = $t;
		}
	
		// retirar os espacos em branco
		$aux = $this->subst($aux,' ','');
	
		return $aux;
	}
  
}  


// Até aqui foi Deus quem me ajudou...

?>
