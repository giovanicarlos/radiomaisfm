<?php
# -------------------------------------------
#
# Trabalhar os arquivos TXT da tabela unificada SUS e retornar um array de dados capaz de ser salvo em cada tabela
# @author Cleiton Andrade <andrade.cleiton@gmail.com>
#
# -------------------------------------------

class ImportComponent extends Component {
	
	public function listarArquivos() {
		$dir = new Folder(APP.'webroot'.DS.'files'.DS.'tabelaunificada');
		$files = $dir->find('.*\.txt');
		$xmlString = APP.'webroot'.DS.'files'.DS.'tabelaunificada'.DS.'defs'.DS.'mapa.xml';
		$xmlArray = Xml::toArray(Xml::build($xmlString));
		foreach($xmlArray['mapa']['tabelas'] as $key => $val) {
			$tabelas[$key] = $val['txt'].'.txt';
			$modelo[$val['txt']] = $val['modelo'];
		}
		$indice = 0;
		foreach($files as $file) {
			$aux = explode('.',$file);
			if (in_array($file,$tabelas)) {
				$retorno[$indice]['arquivo'] = $file;
				$retorno[$indice]['modelo'] = $modelo[$aux[0]];
				$indice++;
			}
		}
		
		// verificar se os arquivos de definição de importação estão presentes 
		return $retorno;
	}

	public function txttoArray($arqTXT,$model) {
		$result = array();
		if ($arqTXT === null) {
			$result[0] = 'Arquivo indefinido...';
		} else {
			$file = explode('.',$arqTXT);
			$xmlString = APP.'webroot'.DS.'files'.DS.'tabelaunificada'.DS.'defs'.DS.$file[0].'.xml';
			$xmlArray = Xml::toArray(Xml::build($xmlString));

			$txtFile = new File(APP.'webroot'.DS.'files'.DS.'tabelaunificada'.DS.$arqTXT, false, 0644);
			$content = $txtFile->read();
			$lines = explode("\r\n",$content);

			$records = array();
			foreach($lines as $key => $line) {
				foreach($xmlArray[$file[0]]['coluna'] as $val) {
					$field = substr($line,($val['inicio'] - 1), $val['tamanho']);
					$records[$key][$model][$val['nome']] = (strlen(trim($field)) > 0 ? $field : null); 
					//$records[$key][$model][$val['nome']] = substr($line,($val['inicio'] - 1),$val['tamanho']);
				} 
			}
			$result = $records;
		}

		return $result;

	}
}
?>
