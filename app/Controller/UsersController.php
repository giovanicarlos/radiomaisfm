<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator','RequestHandler', 'Util', 'Fonetica');

	public function beforeFilter() {
		parent::beforeFilter();

		// For CakePHP 2.1 and up
		// $this->Auth->allow();

		// $this->Auth->allow('initDB'); // We can remove this line after we're finished
	}

	public function initDB() {
		$group = $this->User->Group;

		// Permite aos administradores fazer tudo
		$group->id = 7;
		$this->Acl->allow($group, 'controllers');

		// Permite aos gerentes fazer tudo menos alterar teto fisico/financeiro
		$group->id = 8;
		$this->Acl->deny($group, 'controllers');
		$this->Acl->allow($group, 'controllers/Ent0200000s');
		$this->Acl->allow($group, 'controllers/Ent0205000s');
		$this->Acl->allow($group, 'controllers/Ent0210000s');
		$this->Acl->allow($group, 'controllers/Ent0215000s');
		$this->Acl->allow($group, 'controllers/Ent0220000s');
		$this->Acl->allow($group, 'controllers/Ent0225000s');
		$this->Acl->allow($group, 'controllers/Ent0230000s');
		$this->Acl->allow($group, 'controllers/Ent0235000s');
		$this->Acl->allow($group, 'controllers/Ent0240000s');
		$this->Acl->allow($group, 'controllers/Ent0245000s');

		// Permissao de listar os tetos orçamentarios
		$this->Acl->allow($group, 'controllers/Ent0250000s/index');

		$this->Acl->allow($group, 'controllers/Ent0255000s');
		$this->Acl->allow($group, 'controllers/Ent0260000s');
		$this->Acl->allow($group, 'controllers/Ent0265000s');
		$this->Acl->allow($group, 'controllers/Ent0270000s');
		$this->Acl->allow($group, 'controllers/Ent0275000s');
		$this->Acl->allow($group, 'controllers/Ent0325000s');
		$this->Acl->allow($group, 'controllers/Groups');
		$this->Acl->allow($group, 'controllers/Users');
		$this->Acl->allow($group, 'controllers/Pages');
		
		// Permitir aos usuarios ligados à consorciados fazerem agendamentos no sistema 
		$group->id = 9;
		$this->Acl->deny($group, 'controllers');
		
		$this->Acl->allow($group, 'controllers/Ent0005000s/jsonEnt0005000');
		$this->Acl->allow($group, 'controllers/Ent0010000s/jsonEnt0010000');
		$this->Acl->allow($group, 'controllers/Ent0015000s/jsonEnt0015000');
		$this->Acl->allow($group, 'controllers/Ent0020000s/jsonEnt0020000');
		$this->Acl->allow($group, 'controllers/Ent0025000s/buscar');
		$this->Acl->allow($group, 'controllers/Ent0030000s');
		
		// permissao para cadastrar pacientes via ajax
		$this->Acl->allow($group, 'controllers/Ent0060000s/addPatients');
		
		// permissao para consultar cpf
		$this->Acl->allow($group, 'controllers/Ent0070000s/buscar');

		// permissao para validar Identidade
		$this->Acl->allow($group, 'controllers/Ent0075000s/buscar');

		// permissao para validar Titulo de eleitor
		$this->Acl->allow($group, 'controllers/Ent0085000s/buscar');

		// permissao para validar Numero de reservista
		$this->Acl->allow($group, 'controllers/Ent0090000s/buscar');

		// permissao para validar Carteira Profissional de Trabalho
		$this->Acl->allow($group, 'controllers/Ent0100000s/buscar');

		// permissao para validar Carteira de Habilitacao
		$this->Acl->allow($group, 'controllers/Ent0105000s/buscar');

		$this->Acl->allow($group, 'controllers/Ent0200000s');

		// permissao para visualizar teto financeiro e teto fisico
		$this->Acl->allow($group, 'controllers/Ent0205000s/view');
		$this->Acl->allow($group, 'controllers/Ent0250000s/view');
		$this->Acl->allow($group, 'controllers/Ent0265000s/view');

		// permissao para buscar profissionais
		$this->Acl->allow($group, 'controllers/Ent0210000s/jsonEnt0210000');

		$this->Acl->allow($group, 'controllers/Ent0220000s');

		// permissao somente para alternar entre competencias
		$this->Acl->allow($group, 'controllers/Ent0225000s/changeCompetence');
		
		$this->Acl->allow($group, 'controllers/Ent0230000s');
		$this->Acl->allow($group, 'controllers/Ent0240000s');

		// permissao de pesquisar o financeiro
		$this->Acl->allow($group, 'controllers/Ent0250000s/returnTetoOrcamentario'); 

		// permissao de agendamento
		$this->Acl->allow($group, 'controllers/Ent0255000s/view');
		// permissao de listar as agendas
		$this->Acl->allow($group, 'controllers/Ent0255000s/index');

		$this->Acl->allow($group, 'controllers/Ent0260000s');
		$this->Acl->allow($group, 'controllers/Ent0270000s');

		$this->Acl->allow($group, 'controllers/Ent0275000s/view');
		$this->Acl->allow($group, 'controllers/Ent0275000s/printer');
		$this->Acl->allow($group, 'controllers/Ent0275000s/edit');

		$this->Acl->allow($group, 'controllers/Groups');
		$this->Acl->allow($group, 'controllers/Users');
		$this->Acl->allow($group, 'controllers/Pages');

		// we add an exit to avoid an ugly "missing views" error message
		echo "all done";
		exit;
	}	
	
	
//	public function beforeFilter() {
//		parent::beforeFilter();
//		$this->Auth->allow('login', 'add');
//	}


/**
 * index method
 *
 * @return void
 */
	public function index() {
		$users = $this->User->find('all');
		$this->set('users', $users);
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid registry'));
		}
		$options = array('conditions' => array(
			'User.' . $this->User->primaryKey => $id
			),
			'contain' => array(
				'Ent0060000',
				'Group',
				'Ent0205000'
			) 
		);
		$user = $this->User->find('first', $options);
		if (isset($user['Ent0205000']['ent0125000_id']) and !empty($user['Ent0205000']['ent0125000_id'])) {
			$ent0125000 = $this->User->Ent0205000->Ent0125000->find('first', array(
				'conditions' => array (
					'Ent0125000.id' => $user['Ent0205000']['ent0125000_id']
				),
				'contain' => array (
					'Ent0060000'
				)				
			));
			$user['Ent0205000']['ent0060001'] = $ent0125000['Ent0060000']['ent0060001'];
		}
		$this->set('user', $user);
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->set('title_for_layout', __($this->modelClass)."&nbsp;::&nbsp".__('Add'));
		if ($this->request->is('post')) {
			// nome fonetico do usuario
			$this->request->data['Ent0060000']['ent0060002'] = $this->Fonetica->fonetica($this->request->data['Ent0060000']['ent0060001']); 
			// usuarios sempre serão pessoas fisicas 
			$this->request->data['Ent0060000']['ent0060004'] = 'F';
			// colocar o email na tabela correta
			$this->request->data['Ent0055000'][0]['ent0055001'] = $this->request->data['User']['email'];
			unset($this->request->data['User']['email']);
			// colocar o cpf na tabela correta
			$this->request->data['Ent0065000']['ent0070000_id'] = $this->Util->removeMasks($this->request->data['User']['CPF']);
			unset($this->request->data['User']['CPF']);
			// eliminar o campo de confirmacao de senha
			unset($this->request->data['User']['password_confirm']);
			
			// dados para gravar na tabela de cpf
			$ent0070000['Ent0070000']['id'] = $this->request->data['Ent0065000']['ent0070000_id'];
			
			// gravar primeiro o cpf para nao causar erro de relacionamento
			if ($this->User->Ent0060000->Ent0065000->Ent0070000->save($ent0070000)) {
				// se gravou o cpf entao grava o usuario
				if ($this->User->Ent0060000->saveAll($this->request->data)) {
					return $this->redirect(array('action' => 'index'), 'alert', array('plugin' => 'BoostCake', 'class' => 'alert-success'));
				} else {
					$this->Session->setFlash(__('The registry could not be saved. Please, try again.'), 'alert', array('plugin' => 'BoostCake', 'class' => 'alert-danger'));
				} 
			} else {
				$this->Session->setFlash(__('The registry could not be saved. Please, try again.'), 'alert', array('plugin' => 'BoostCake', 'class' => 'alert-danger'));
			}
		}
		$groups = $this->User->Group->find('list');
		$this->set(compact('groups'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->User->exists($id)) {
			throw new NotFoundException(__('Invalid registry'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->User->save($this->request->data)) {
				$this->Session->setFlash(__('The registry has been saved.'), 'alert', array('plugin' => 'BoostCake', 'class' => 'alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The registry could not be saved. Please, try again.'), 'alert', array('plugin' => 'BoostCake', 'class' => 'alert-error'));
			}
		} else {
			$options = array(
				'conditions' => array(
					'User.' . $this->User->primaryKey => $id
				),
				'contain' => array(
					'Ent0060000',
					'Group',
					'Ent0205000'
				)
			);
			$this->request->data = $this->User->find('first', $options);
			$user = $this->User->find('first',$options);
			$ent0055000 = $this->User->Ent0060000->Ent0055000->find('first', array(
					'conditions' => array(
						'Ent0055000.ent0060000_id' => $user['User']['ent0060000_id']
					),
					'recursive' => -1
				)
			);
			if (count($ent0055000) > 0) {
				$user['User']['email'] = $ent0055000['Ent0055000']['ent0055001'];
			}
			$this->request->data = $user;
		}
		$groups = $this->User->Group->find('list');

		$intercropping =  $this->User->Ent0205000->Ent0125000->find('all', array(
				'order' => 'Ent0060000.ent0060001',
				'conditions' => array(
					'not' => array( 'Ent0205000.ent0125000_id' => null) 
				),
				'fields' => array('Ent0205000.id','Ent0060000.ent0060001'),
				'recursive' => 0
		));

		if (count($intercropping) > 0) {
			foreach($intercropping as $val) {
				$ent0205000s[$val['Ent0205000']['id']] = $val['Ent0060000']['ent0060001'];				
			}
		} else {
			$ent0205000s = null;
		}
		$this->set(compact('ent0060000s', 'groups', 'ent0205000s'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->User->id = $id;
		if (!$this->User->exists()) {
			throw new NotFoundException(__('Invalid registry'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->User->delete()) {
			$this->Session->setFlash(__('The registry has been deleted.'), 'alert', array('plugin' => 'BoostCake', 'class' => 'alert-success'));
		} else {
			$this->Session->setFlash(__('The registry could not be deleted. Please, try again.'), 'alert', array('plugin' => 'BoostCake', 'class' => 'alert-error'));
		}
		return $this->redirect(array('action' => 'index'));
	}

	public function login() {
		$this->set('title_for_layout', __('Users')."&nbsp;::&nbsp".__('Login'));
		$this->layout = 'login';
		if($this->request->is('post')) {
			if($this->Auth->login()) {
				$loginUser = AuthComponent::user();
				if (!isset($loginUser['Ent0225000']['id']) or is_null($loginUser['Ent0225000']['id'])) {
					$ent0225000_id = date('mY');

					App::uses('Ent0225000','Model');
					$ent0225000 = new Ent0225000();
					$ent0225000->recursive = -1;
					$competencia = $ent0225000->findById($ent0225000_id);
					
					if (count($competencia) > 0) {
						$this->Session->write('Auth.User.Ent0225000.id', $competencia['Ent0225000']['id']);
						$this->Session->write('Auth.User.Ent0225000.ent0225002', $competencia['Ent0225000']['ent0225002']);
						return $this->redirect($this->Auth->redirect());
					} else {
						return $this->redirect(array('controller' => 'ent0225000s', 'action' => 'changeCompetence'));
					}
				
				}				

				return $this->redirect($this->Auth->redirect());
			} else {
				$this->Session->setFlash('Usuário e/ou senha estão incorretos', 'default', array(), 'auth');
			}
        }
	}

	public function logout() {
    	$this->redirect($this->Auth->logout());
	}
	
	public function control() {
	
	}

	public function changeStatus($id = null) {
		$this->layout = 'ajax';
		if (!$id) {
			$this->Session->setFlash(__('Identificação Inválida', true), 'alert', array('plugin' => 'BoostCake', 'class' => 'alert-error'));
			$this->redirect(array('action' => 'index'));
		}
		
		$this->User->recursive = -1;
		$user = $this->User->read(null, $id);
		$status = ($user['User']['status'] == '1') ? '0' : '1';
		$this->User->updateAll(array('User.status' => "'".$status."'"), array('User.id' => $id)); 
		$this->redirect(array('action' => 'index'));
	}
}
