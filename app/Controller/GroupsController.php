<?php
App::uses('AppController', 'Controller');
/**
 * Groups Controller
 *
 * @property Group $Group
 * @property PaginatorComponent $Paginator
 */
class GroupsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');
	
//	public function beforeFilter() {
//		parent::beforeFilter();

		// For CakePHP 2.1 and up
//		$this->Auth->allow();
//	}

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->set('title_for_layout', __($this->name)."&nbsp;::&nbsp".__('List'));
		$this->Group->recursive = 0;
		$this->set('groups', $this->Paginator->paginate());
//		$this->set('groups', $this->Group->find('all'));
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		$this->set('title_for_layout', __($this->modelClass)."&nbsp;::&nbsp".__('View'));
		if (!$this->Group->exists($id)) {
			throw new NotFoundException(__('Invalid registry'));
		}
		$options = array('conditions' => array('Group.' . $this->Group->primaryKey => $id));
		$group = $this->Group->find('first', $options);
		
		// nome dos usuario do grupo
		/* if (count($group['User']) > 0) {
			foreach ($group['User'] as $key => $val) {
				$user = $this->Group->User->Ent0060000->find('first', array(
					'conditions' => array(
						'Ent0060000.id' => $val['ent0060000_id']
					),
					'recursive' => -1
				));
				$group['User'][$key]['ent0060001'] = $user['Ent0060000']['ent0060001'];
			}
		} */
		$this->set('group', $group); 
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		$this->set('title_for_layout', __($this->modelClass)."&nbsp;::&nbsp".__('Add'));
		if ($this->request->is('post')) {
			$this->Group->create();
			if ($this->Group->save($this->request->data)) {
				$this->Session->setFlash(__('The registry has been saved.'), 'alert', array('plugin' => 'BoostCake', 'class' => 'alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The registry could not be saved. Please, try again.'), 'alert', array('plugin' => 'BoostCake', 'class' => 'alert-danger'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		$this->set('title_for_layout', __($this->modelClass)."&nbsp;::&nbsp".__('Edit'));
		if (!$this->Group->exists($id)) {
			throw new NotFoundException(__('Invalid registry'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Group->save($this->request->data)) {
				$this->Session->setFlash(__('The registry has been saved.'), 'alert', array('plugin' => 'BoostCake', 'class' => 'alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The registry could not be saved. Please, try again.'), 'alert', array('plugin' => 'BoostCake', 'class' => 'alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Group.' . $this->Group->primaryKey => $id));
			$this->request->data = $this->Group->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Group->id = $id;
		if (!$this->Group->exists()) {
			throw new NotFoundException(__('Invalid registry'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Group->delete()) {
			$this->Session->setFlash(__('The registry has been deleted.'), 'alert', array('plugin' => 'BoostCake', 'class' => 'alert-success'));
		} else {
			$this->Session->setFlash(__('The registry could not be deleted. Please, try again.'), 'alert', array('plugin' => 'BoostCake', 'class' => 'alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
